# coding=utf-8
"""
Django settings for Leadhints project.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""
from django.utils.translation import ugettext_lazy as _
import os.path
from urlparse import urljoin
import sys


BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

INSTALLED_APPS = (
    'djangocms_admin_style',
    'djangocms_text_ckeditor',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'djangocms_style',
    'djangocms_column',
    'djangocms_googlemap',
    'djangocms_inherit',
    'reversion',
    'widget_tweaks',
    'filer',
    'easy_thumbnails',
    'cmsplugin_filer_file',
    'cmsplugin_filer_folder',
    'cmsplugin_filer_link',
    'cmsplugin_filer_image',
    'cmsplugin_filer_teaser',
    'cmsplugin_filer_video',
    'meta',
    'meta_mixin',
    'funeral_finder_cms',
    'catalog',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
)

SITE_ID = 1

ROOT_URLCONF = 'funeral_finder_cms.urls'

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [
        os.path.join(BASE_DIR, 'templates'),
    ],
    'APP_DIRS': False,
    'OPTIONS': {
        'context_processors': [
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
            'django.core.context_processors.i18n',
            'django.core.context_processors.debug',
            'django.core.context_processors.request',
            'django.core.context_processors.media',
            'django.core.context_processors.csrf',
            'django.core.context_processors.tz',
            'sekizai.context_processors.sekizai',
            'django.core.context_processors.static',
            'cms.context_processors.cms_settings',
            'catalog.context_processors.companies_alphabet',
            'funeral_finder_cms.context_processors.login_form',
        ],
        'loaders': [
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
            'django.template.loaders.eggs.Loader'
        ],
    },
}, ]

ADMINS = (
    ('Rinat', 'rinaatt@gmail.com'),
)
MANAGERS = ADMINS

WSGI_APPLICATION = 'funeral_finder_cms.wsgi.application'

# This is defined here as a do-nothing function because we can't import
# django.utils.translation -- that module depends on the settings.

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGES = (
    ('de', u'Deutsche'),
)
LANGUAGE_CODE = 'de'
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
    os.path.join(BASE_DIR, 'apps', 'advisor', 'locale'),
)

TIME_ZONE = 'Europe/Berlin'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LOGIN_URL = '/auth/login/'
LOGOUT_URL = '/auth/logout/'
LOGIN_REDIRECT_URL = '/'
LOGIN_EXEMPT_URLS = (
    r'admin/',
    r'^favicon.ico$',
    r'^media/favicon.ico$',
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    # important! place right before django.contrib.staticfiles.finders.AppDirectoriesFinder
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'public', 'static'),
)

SESSION_ENGINE = 'redis_sessions.session'
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'
SESSION_REDIS_DB = 0
SESSION_REDIS_PREFIX = 'session'

# Celery
# BROKER_URL = 'redis+socket:///var/run/redis/redis.sock'
BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost/0'
CELERY_ACCEPT_CONTENT = ('json', 'msgpack', 'yaml', 'pickle')
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'
#
# # from datetime import timedelta
# from celery.schedules import crontab
#
# CELERYBEAT_SCHEDULE = {
#     'sync-every-3-minutes': {
#         'task': 'billing.tasks.sync_with_mysql',
#         'schedule': crontab(minute='0-59/3'),
#         # 'args': (16, 16)
#     },
#     'complete-every-3-minutes': {
#         'task': 'billing.tasks.complete_campaigns',
#         'schedule': crontab(minute='1-59/3'),
#     }
# }

CMS_LANGUAGES = {
    'default': {
        'public': True,
        'hide_untranslated': False,
        'redirect_on_fallback': False,
    },
}

CMS_TEMPLATES = (
    ('page.html', u'Page'),
    ('startpage.html', u'Start page'),
    ('advisor_page.html', u'Ratgeber page')
)

CMS_PERMISSION = True

CMS_PLACEHOLDER_CONF = {}

MIGRATION_MODULES = {
    'djangocms_column': 'djangocms_column.migrations_django',
    'djangocms_googlemap': 'djangocms_googlemap.migrations_django',
    'djangocms_inherit': 'djangocms_inherit.migrations_django',
    'djangocms_style': 'djangocms_style.migrations_django',
    'cmsplugin_filer_file': 'cmsplugin_filer_file.migrations_django',
    'cmsplugin_filer_folder': 'cmsplugin_filer_folder.migrations_django',
    'cmsplugin_filer_link': 'cmsplugin_filer_link.migrations_django',
    'cmsplugin_filer_image': 'cmsplugin_filer_image.migrations_django',
    'cmsplugin_filer_teaser': 'cmsplugin_filer_teaser.migrations_django',
    'cmsplugin_filer_video': 'cmsplugin_filer_video.migrations_django',
}

META_SITE_PROTOCOL = 'http'
META_SITE_DOMAIN = 'bestattungshelfer.de'

CONTACT_EMAIL = 'contact@bestattungshelfer.de'
DEFAULT_FROM_EMAIL = 'info@bestattungshelfer.de'
EMAIL_SUBJECT_PREFIX = '[BestattungShelfer24]'


THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    # 'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

GOOGLE_STATIC_MAP_DOMAIN = 'maps.googleapis.com'
GOOGLE_STATIC_MAP_PATH = '/maps/api/staticmap'
GOOGLE_MAP_ZOOM = 15
GOOGLE_MAP_GENERAL_PARAMS = {
    'zoom': GOOGLE_MAP_ZOOM,
    'size': '286x216',
    'language': LANGUAGE_CODE,
    'sensor': False,
    'key': '',
}
GOOGLE_MAP_SECRET = False

TEXT_SAVE_IMAGE_FUNCTION = 'cmsplugin_filer_image.integrations.ckeditor.create_image_plugin'
CKEDITOR_SETTINGS = {
    'bodyClass': 'post-detail',
    'contentsCss': '/static/css/contents.css',
    'stylesSet': 'default:/static/js/ckeditor_styles.js',
}

FILER_LINK_STYLES = (
    (" ", "Default"),
    ("btn-link", "Button Link"),
)
