from os import environ
import os.path
from .base import *

DEBUG = False

ALLOWED_HOSTS = ['.bestattungshelfer.de', '.bestattungshelfer.de.',
                 'lvps92-51-161-175.dedicated.hosteurope.de', 'localhost']

SECRET_KEY = environ['FUNERALFINDER_SECRET_KEY']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'funeralfinder_cms',
        'USER': 'django',
        'PASSWORD': environ['FUNERALFINDER_DB_PASS'],
    }
}

SESSION_REDIS_UNIX_DOMAIN_SOCKET_PATH = '/var/run/redis/redis.sock'

try:
    virtualenv_root = os.environ['VIRTUAL_ENV']
except KeyError:
    sys.stderr.write('Error: virtualenv does not activated.\n')
    sys.exit(1)

# VAR_ROOT = os.path.join(virtualenv_root, 'var')
PUB_ROOT = os.path.join(virtualenv_root, 'var', 'public')
STATIC_ROOT = os.path.join(PUB_ROOT, 'static')
MEDIA_ROOT = os.path.join(PUB_ROOT, 'media')

# SERVER_EMAIL = 'django.cases@gmail.com'
# DEFAULT_FROM_EMAIL = 'django.cases@gmail.com'

SEND_EMAILS = True
EMAIL_HOST = 'vwp15571.webpack.hosteurope.de'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'wp12349287-info'
EMAIL_HOST_PASSWORD = 'cra8twick5'

EMAIL_FAIL_SILENTLY = True

GOOGLE_MAP_GENERAL_PARAMS['key'] = 'AIzaSyChHGm7UC9TCCMay3rndr59M7sj1ffOjTM'
GOOGLE_MAP_SECRET = 'wxkJRK6_3_LYfoA6zJpt1JMrk2w='

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(virtualenv_root, 'var', 'log', 'django.log'),
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['mail_admins', 'file'],
            'level': 'ERROR',
            'propagate': False,
        },
        'funeral_finder_cms': {
            'handlers': ['console', 'file'],
            'level': 'INFO',
        },
    }
}

try:
    from .local import *
except ImportError:
    pass
