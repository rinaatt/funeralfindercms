from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

if 'celeryd' in sys.argv:
    DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '32l4%meevk!2_ylt44ec6=xwfresgrp$_rxc!2f0x6h(swt%bm'

PUB_ROOT = os.path.join(BASE_DIR, 'public')
STATIC_ROOT = ''
MEDIA_ROOT = os.path.join(PUB_ROOT, 'media')

ALLOWED_HOSTS = [
    '.bestattungshelfer.local',  # Allow domain and subdomains
    '.bestattungshelfer.local.',  # Also allow FQDN and subdomains
    'localhost',
]

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'funeralfinder_cms',
        'USER': 'django',
        'PASSWORD': 'djangopassword',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}

LANGUAGES = (('en', u'English'), )
LANGUAGE_CODE = 'en'
PARLER_DEFAULT_LANGUAGE_CODE = 'en'

SESSION_REDIS_HOST = 'localhost'
SESSION_REDIS_PORT = 6379

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

GOOGLE_MAP_GENERAL_PARAMS['key'] = 'AIzaSyAN65lR1dhPVUJWPYHob0artGWXjQ6vofg'
GOOGLE_MAP_SECRET = 'x4Wv8UNkENrWFVKyIDKBts5fRgQ='

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'funeral_finder_cms': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    }
}


# django-debug-toolbar
# INSTALLED_APPS += (
#     'debug_toolbar',
# )
# INTERNAL_IPS = ['127.0.0.1', '192.168.56.1', ]
# DEBUG_TOOLBAR_PATCH_SETTINGS = False
# MIDDLEWARE_CLASSES = (
#     # 'django_hosts.middleware.HostsRequestMiddleware',
#     'django.contrib.sessions.middleware.SessionMiddleware',
#     'debug_toolbar.middleware.DebugToolbarMiddleware',
#     'django.middleware.csrf.CsrfViewMiddleware',
#     'django.contrib.auth.middleware.AuthenticationMiddleware',
#     'django.contrib.messages.middleware.MessageMiddleware',
#     'django.middleware.locale.LocaleMiddleware',
#     'django.middleware.common.CommonMiddleware',
#     'django.middleware.clickjacking.XFrameOptionsMiddleware',
#     'cms.middleware.user.CurrentUserMiddleware',
#     'cms.middleware.page.CurrentPageMiddleware',
#     'cms.middleware.toolbar.ToolbarMiddleware',
#     'cms.middleware.language.LanguageCookieMiddleware',
#     # 'django_hosts.middleware.HostsResponseMiddleware',
# )
# DEBUG_TOOLBAR_CONFIG = {
#     'SHOW_TOOLBAR_CALLBACK': 'funeral_finder_cms.settings.show_debug_toolbar',
#     'JQUERY_URL': '//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js',
# }
#
# import re
# disabled_debug_toolbar = (
#     re.compile(r'/admin/'),
#     re.compile(r'/ckeditor/'),
# )
#
#
# def show_debug_toolbar(request):
#     """
#     Check if show debug toolbar
#     @param request: django http request
#     @type request: django.http.request.HttpRequest
#     @return: boolean value
#     @rtype: bool
#     """
#     from debug_toolbar.middleware import show_toolbar
#     is_show = show_toolbar(request)
#     # is_admin_host = 'admin' in request.get_host()
#     # is_show = is_show and not is_admin_host
#     uri = request.path
#     check_by_url = all(
#         not bool(patt.search(uri))
#         for patt in disabled_debug_toolbar)
#     return is_show and check_by_url


CELERY_HIJACK_ROOT_LOGGER = False

try:
    from .local import *
except ImportError:
    pass
