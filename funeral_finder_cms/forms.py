# coding=utf-8
import hashlib
import string
import django.contrib.auth.forms as d_af
from django.conf import settings
from catalog.models import Company
import django.forms as d_f
from django.core import mail as d_mail
from django.core.urlresolvers import reverse
import django.contrib.auth as d_a
from django.utils.text import capfirst
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.contrib.auth.tokens import default_token_generator
from django.core.urlresolvers import reverse

__author__ = 'Rinat'

NEW_PROVIDER = 'new_provider'
INVALID_LOGIN = 'invalid_login'
INVALID_PASSWORD = 'invalid_password'
INACTIVE_USER = 'inactive'
USER_REGISTERED = 'user_created'
USER_NOT_EXISTS = 'no_user'
NO_PASSWORD = 'no_password'
NO_USERNAME = 'no_mail'
RESET_PASSWORD = 'reset_password'

MAIL_SUBJECT = u'Zugangsdaten für Bestattungshelfer.de'
MAIL_TEMPLATE_PLAIN = u'''
Sehr geehrte Damen und Herren, veielen Dank für Ihre Registrierung bei bestattungshelfer.de.

Ihre Zugangsdaten lauten:
    E-Mail: {user_mail}
    Passwort: {user_passw}

Profitieren Sie von der kostenfreien Kunden-Gewinnung und pflegen Sie Ihr Firmenprofil
hier: {profile_url}

Mit freundlichen Grüßen

Ihr bestattungshelfer.de-Team

Bestattungshelfer24 GmbH
Holzhauser Straße 177
13509 Berlin

Geschäftsführer: Christian Metzger
EMail: info@bestattungshelfer.de
Handelsregister: Charlottenburg (Berlin) HRB 157880 B
'''

MAIL_TEMPLATE_HTML = u'''
<p>Sehr geehrte Damen und Herren, veielen Dank für
   Ihre Registrierung bei <a href="http://bestattungshelfer.de/">bestattungshelfer.de</a>.</p>
<p>Ihre Zugangsdaten lauten:</p>
<ul>
    <li><b>E-Mail:</b> {user_mail}</li>
    <li><b>Passwort:</b> {user_passw}</li>
</ul>
<p>Profitieren Sie von der kostenfreien Kunden-Gewinnung und
   pflegen Sie Ihr Firmenprofil <a href="{profile_url}">hier</a>.</p>
<p>Mit freundlichen Grüßen</p>
<p>Ihr bestattungshelfer.de-Team</p>
<hr/>
<address>
Bestattungshelfer24 GmbH<br/>
Holzhauser Straße 177<br/>
13509 Berlin
</address>
<hr/>
<address>
<i>Geschäftsführer:</i> Christian Metzger<br/>
<i>EMail:</i> <a href="mailto:info@bestattungshelfer.de">info@bestattungshelfer.de</a><br/>
<i>Handelsregister:</i> Charlottenburg (Berlin) HRB 157880 B
</address>
'''


def send_mail(user_mail, msg_text, msg_html=None, subj=MAIL_SUBJECT):
    if msg_html is None:
        email_klass = d_mail.EmailMessage
    else:
        email_klass = d_mail.EmailMultiAlternatives
    email = email_klass(subj, msg_text)
    email.to.append(user_mail)
    email.reply_to.append(settings.CONTACT_EMAIL)
    if msg_html is not None:
        email.attach_alternative(msg_html, 'text/html')
    email.send()


class EmailAuthenticateForm(d_af.AuthenticationForm):
    pass_remind = d_f.BooleanField(required=False, widget=d_f.HiddenInput)
    username = d_f.EmailField(max_length=254, label=u'E-mail adresse')

    error_messages = {
        # INVALID_LOGIN: u'Bitte geben Sie eine gültige Email-Adresse ein',
        INVALID_LOGIN: u'Bitte geben Sie eine korrekte Email-Adresse und Ihr Passwort. '
                       u'Bedenke, dass möglicherweise Groß-und Kleinschreibung in beiden '
                       u'Feldern beachtet werden muss.',
        INACTIVE_USER: u'Dieses Konto ist inaktiv.',
        USER_REGISTERED: u'Vielen Dank für Ihre Registrierung. Wir senden'
                         u' Ihnen Ihre Zugangsdaten an Ihre unten angegebene'
                         u' Email-Adresse',
        USER_NOT_EXISTS: u'Benutzer nicht existiert',
        RESET_PASSWORD: u'Ihr Password wurde Ihnen an Ihre bei uns'
                        u' registrierte Email-Adresse zu geschickt',
        NO_PASSWORD: u'Bitte geben Sie Ihr Passwort ein.',
        NO_USERNAME: u'Bitte geben Sie eíne gültige Email-Adresse und Ihr Passwort ein',
        NEW_PROVIDER: u'Schicken Sie uns bitte eine Mail mit Ihren Kontaktdaten an '
                      u'<a href="mailto:info@bestattungshelfer.de">info@bestattungshelfer.de</a>',
        INVALID_PASSWORD: u'Bitte geben Sie Ihr korrektes Passwort ein'
    }
    digs = string.digits + string.lowercase

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        super(EmailAuthenticateForm, self).__init__(request, *args, **kwargs)
        self.UserModel = d_a.get_user_model()
        self.UserModel.USERNAME_FIELD = 'email'
        # Set the label for the "username" field.
        self.username_field = self.UserModel._meta.get_field(self.UserModel.USERNAME_FIELD)

    def _send_mail(self, user_mail, user_passw):
        format_params = dict(
            user_mail=user_mail,
            user_passw=user_passw,
            profile_url=self.request
            .build_absolute_uri(reverse('profile_firms'))
        )
        send_mail(
            user_mail,
            MAIL_TEMPLATE_PLAIN.format(**format_params),
            MAIL_TEMPLATE_HTML.format(**format_params))

    def hash_mail(self, email):
        digest = hashlib.md5(email).hexdigest()
        x = int(digest, 16)
        base = len(self.digs)
        if not x:
            return '0'
        sign = -1 if x < 0 else 1
        x = abs(x)
        digits = []
        while x:
            digits.append(self.digs[x % base])
            x /= base
        if sign < 0:
            digits.append('-')
        digits.reverse()
        return ''.join(digits)

    def _register_exist_provider(self, user_mail):
        user_manager = getattr(self.UserModel, '_default_manager')
        """@type: django.contrib.auth.models.UserManager"""
        user_passw = user_manager.make_random_password(length=8).encode('utf-8')
        new_user = user_manager.create_user(
            self.hash_mail(user_mail), email=user_mail, password=user_passw)
        """@type: django.contrib.auth.models.User"""
        new_user.set_password(user_passw)
        new_user.save()
        Company.companies.filter(mail__iexact=user_mail).update(user=new_user)
        self._send_mail(user_mail, user_passw)

    def _reset_password(self, user_mail):
        user_manager = getattr(self.UserModel, '_default_manager')
        user = user_manager.filter(email=user_mail).first()
        """@type: django.contrib.auth.models.User"""
        user_passw = user_manager.make_random_password(length=8).encode('utf-8')
        user.set_password(user_passw)
        user.save()
        self._send_mail(user.email, user_passw)

    def clean(self):
        user_mail = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        pass_remind = self.cleaned_data.get('pass_remind')
        user_manager = getattr(self.UserModel, '_default_manager')
        """@type: django.contrib.auth.models.UserManager"""
        error_code = None
        params = None
        if user_mail:
            user_exist = user_manager.filter(email__iexact=user_mail).exists()
            company_exists = Company.companies.filter(mail__iexact=user_mail).exists()
            if pass_remind:
                # registration or forgot password
                if user_exist:
                    # forgot passwond
                    self._reset_password(user_mail)
                    error_code = RESET_PASSWORD
                else:
                    # registration
                    if company_exists:
                        # register exist provider
                        self._register_exist_provider(user_mail)
                        error_code = USER_REGISTERED
                    else:
                        # register new company
                        error_code = NEW_PROVIDER
            else:
                # attepmt login
                if user_exist:
                    self.user_cache = d_a.authenticate(password=password, email=user_mail)
                    if self.user_cache is None:
                        error_code = INVALID_LOGIN if password else NO_PASSWORD
                else:
                    error_code = INVALID_LOGIN
        else:
            error_code = NO_USERNAME
        if error_code is not None:
            raise d_f.forms.ValidationError(
                self.error_messages[error_code], code=error_code, params=params)
        return self.cleaned_data
