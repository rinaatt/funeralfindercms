# coding=utf-8
from .forms import EmailAuthenticateForm

__author__ = 'Rinat'


def login_form(request):
    """
    Context processor for login form
    @param request: http request object
    @type request: django.http.request.HttpRequest
    @return: context dict
    @rtype: dict
    """
    context = {}
    if not request.user.is_authenticated():
        context.update(login_form=EmailAuthenticateForm())
    return context
