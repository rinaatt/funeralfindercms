# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('funeral_finder_cms', '0002_auto_20150911_0952'),
    ]

    operations = [
        migrations.CreateModel(
            name='DownloadFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('file', models.FileField(upload_to=b'downloads/')),
                ('is_enabled', models.BooleanField(default=True)),
            ],
        ),
    ]
