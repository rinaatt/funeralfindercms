# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


def insert_plugin(apps, schema_editor):
    from cms.models.pagemodel import Page
    from cms.api import add_plugin, publish_page
    from django.apps import apps as d_apps
    Page.objects.filter(template=b'startpage.html').update(reverse_id=b'main')
    page = Page.objects.drafts().filter(reverse_id=b'main').first()
    placeholder = page.placeholders.get(slot='downloaded_files')
    add_plugin(placeholder, 'CMSDownoladFilePlugin', 'de')
    User = d_apps.get_model(settings.AUTH_USER_MODEL)
    admin = User.objects.filter(is_superuser=True).first()
    publish_page(page, admin, 'de')


class Migration(migrations.Migration):

    dependencies = [
        (b'funeral_finder_cms', b'0003_downloadfile'),
    ]

    operations = [
        migrations.RunPython(insert_plugin)
    ]
