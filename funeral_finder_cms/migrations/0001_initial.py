# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings

from cms.api import create_page, add_plugin, publish_page, create_title
from django.contrib.auth.models import User
from django.contrib.sites.apps import create_default_site


def add_pages(apps, schema_editor):
    create_default_site(apps.get_app_config('sites'))
    page = create_page('Main', 'startpage.html', 'de')
    placeholder_f = page.placeholders.get(slot='form')
    add_plugin(placeholder_f, 'SearchFormPlugin', 'de')
    placeholder_t = page.placeholders.get(slot='first_teaser')
    add_plugin(placeholder_t, 'ArticlesTeaserPlugin', 'de')
    try:
        user = User.objects.filter(is_superuser=True).first()
    except User.DoesNotExists:
        print("I need superuser!")
    else:
        publish_page(page, user, 'de')


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        (b'sites', b'__latest__'),
        (b'cms', b'__latest__'),
        (b'menus', b'__latest__'),
    ]

    operations = [
        migrations.CreateModel(
            name='TeaserExtension',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('show_as_main', models.BooleanField(default=False)),
                ('extended_object', models.OneToOneField(editable=False, to='cms.Page')),
                ('public_extension', models.OneToOneField(related_name='draft_extension', null=True, editable=False,
                                                          to='funeral_finder_cms.TeaserExtension')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='teaserextension',
            name='show_as_other',
            field=models.BooleanField(default=False, verbose_name='As other article'),
        ),
        migrations.AlterField(
            model_name='teaserextension',
            name='show_as_main',
            field=models.BooleanField(default=False, verbose_name='As main article'),
        ),
        migrations.RunPython(add_pages),
    ]
