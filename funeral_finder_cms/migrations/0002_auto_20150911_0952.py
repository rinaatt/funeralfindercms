# coding=utf-8
from __future__ import unicode_literals

from django.db import models, migrations
from cms.api import create_page, add_plugin, publish_page, create_title
from django.contrib.auth.models import User


def add_pages(apps, schema_editor):
    page = create_page(
        u'Ratgeber', 'page.html', 'de', slug='ratgeber',
        in_navigation=True, reverse_id='adv')
    try:
        user = User.objects.filter(is_superuser=True).first()
    except User.DoesNotExists:
        print("I need superuser!")
    else:
        publish_page(page, user, 'de')


class Migration(migrations.Migration):

    dependencies = [
        ('funeral_finder_cms', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_pages)
    ]
