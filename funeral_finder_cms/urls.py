# coding=utf-8
from __future__ import print_function
from cms.sitemaps import CMSSitemap
from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib.admin import site as admin_site
from django.contrib.auth import urls as auth_urls
from django.contrib.auth import logout
from django.contrib.sitemaps.views import sitemap
from django.conf import settings
from django.views.static import serve
from django.views.decorators.csrf import csrf_exempt
from .views import XhrView, account_activate
from catalog.views import ProfileFirmsView, FirmEditView
# admin.autodiscover()


def dummy(r):
    pass


# urlpatterns = i18n_patterns(
urlpatterns = [
    url(r'^admin/', include(admin_site.urls)),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^xhr/(?P<action>\w+)$', csrf_exempt(XhrView.as_view()), name='xhr'),
    url(r'^catalog/', include('catalog.urls', namespace='catalog', app_name='catalog')),
    # url(r'^auth/logout-when-redirect'),
    url(r'^auth/', include(auth_urls, namespace='auth')),
    url(r'^accounts/profile-firms$', ProfileFirmsView.as_view(), name='profile_firms'),
    url(r'^accounts/profile-firms/(?P<pk>\d+)$', FirmEditView.as_view(), name='firm_edit'),
    url(r'^accounts/registration/(?P<who>\w+)', dummy, name='registration'),
    url(r'^accounts/activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})$',
        account_activate, name='account_activate'),
    # url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
    # url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]
# )

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += [
        url(r'^media(?P<path>.*)$', serve,
            kwargs={'document_root': settings.MEDIA_ROOT,
                    'show_indexes': True}),
    ] + staticfiles_urlpatterns()
    try:
        import debug_toolbar
    except ImportError:
        pass
    else:
        urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]

urlpatterns += [
    url(r'^(?P<path>.+(\.ico|\.png|\.gif|\.jpg|\.jpeg|\.txt))$', serve,
        kwargs={'document_root': settings.PUB_ROOT}, name='public'),
    url(r'^', include('cms.urls')),
]
