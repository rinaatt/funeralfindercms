# coding=utf-8
from django.db import models
from cms.extensions import PageExtension
from cms.extensions.extension_pool import extension_pool


class DownloadFile(models.Model):
    title = models.CharField(max_length=100)
    file = models.FileField(upload_to='downloads/')
    is_enabled = models.BooleanField(default=True)

    @property
    def url(self):
        return self.file.url

    def __unicode__(self):
        return self.title


class TeaserExtension(PageExtension):
    show_as_main = models.BooleanField(u'As main article', default=False, blank=True)
    show_as_other = models.BooleanField(u'As other article', default=False, blank=True)


extension_pool.register(TeaserExtension)
