# coding=utf-8
from django.core.paginator import Paginator
import django.utils.log as d_log


log = d_log.getLogger(__name__)


class CustomPaginator(Paginator):
    curr_page_number = None

    def validate_number(self, number):
        self.curr_page_number = super(CustomPaginator, self).validate_number(number)
        return self.curr_page_number

    def _get_page_range(self):
        log.debug('curr_page_number: %d', self.curr_page_number)
        if self.num_pages > 10:
            if self.curr_page_number < 5:
                start = 1
                finish = self.num_pages if self.num_pages < 10 else 9
            elif self.curr_page_number > self.num_pages - 4:
                start = self.num_pages - 8 if self.num_pages > 8 else 1
                finish = self.num_pages
            else:
                start = self.curr_page_number - 3
                finish = self.curr_page_number + 5
        else:
            start = 1
            finish = self.num_pages
        return range(start, finish + 1)
    page_range = property(_get_page_range)
