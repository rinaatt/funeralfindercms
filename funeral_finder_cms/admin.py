# coding=utf-8

from django.contrib import admin
from cms.extensions import PageExtensionAdmin
from .models import DownloadFile, TeaserExtension


@admin.register(DownloadFile)
class DownloadFileAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_enabled', )


@admin.register(TeaserExtension)
class TeaserExtensionAdmin(PageExtensionAdmin):
    pass
