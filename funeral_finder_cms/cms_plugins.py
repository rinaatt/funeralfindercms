# coding=utf-8
from .models import DownloadFile, TeaserExtension
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
# from cms.models.pluginmodel import CMSPlugin


class CMSDownoladFilePlugin(CMSPluginBase):
    module = u'FuneralFinder'
    name = u'Download Files'
    render_template = "plugins/download_files.html"

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
            'files': DownloadFile.objects.filter(is_enabled=True)
        })
        return context


plugin_pool.register_plugin(CMSDownoladFilePlugin)  # register the plugin


class ArticlesTeaserPlugin(CMSPluginBase):
    module = u'FuneralFinder'
    name = u'Articles teaser'
    render_template = 'plugins/articles_teaser.html'

    def _get_plugin_instance(self, page, slot_name, plugin_type):
        placeholder = page.placeholders.get(slot=slot_name)
        """@type: cms.models.placeholdermodel.Placeholder"""
        plugin = placeholder.cmsplugin_set\
            .filter(plugin_type=plugin_type).first()
        """@type: cms.models.pluginmodel.CMSPlugin"""
        if plugin is None:
            return None
        else:
            plugin_instance, plugin_class = plugin.get_plugin_instance()
            return plugin_instance

    def _get_data(self, page):
        """@type page: cms.models.pagemodel.Page"""
        return {
            'title': page.get_title(),
            'url': page.get_absolute_url(),
            'image': self._get_plugin_instance(
                page, u'article_header', 'FilerImagePlugin'),
            'text': self._get_plugin_instance(
                page, u'article_content', 'TextPlugin')
        }

    def render(self, context, instance, placeholder):
        ext_object = TeaserExtension.objects.filter(
            show_as_main=True, extended_object__publisher_is_draft=False
        ).first()
        objects_qs = TeaserExtension.objects.filter(
            show_as_other=True, extended_object__publisher_is_draft=False)
        others = []
        for obj in objects_qs[0:3]:
            others.append(self._get_data(obj.get_page()))
        ctx = {
            'instance': instance,
            'other_articles': others,
        }
        if ext_object:
            ctx['main_article'] = self._get_data(ext_object.get_page())
        context.update(ctx)
        return context

plugin_pool.register_plugin(ArticlesTeaserPlugin)


class ChildArticlesPlugin(CMSPluginBase):
    module = u'FuneralFinder'
    name = u'Child Articles'
    render_template = 'plugins/child_articles.html'

    def render(self, context, instance, placeholder):
        context = super(ChildArticlesPlugin, self).render(context, instance, placeholder)
        req = context['request']
        current_page = instance.placeholder.page
        """@type: cms.models.pagemodel.Page"""
        from cms.models.pagemodel import Page
        context['curr_page_id'] = current_page.id
        child_pages = []
        for page in Page.objects.public().filter(parent_id=current_page.id):
            page_data = {'title': page.get_title(), 'url': page.get_absolute_url()}
            placeholder = page.placeholders.get(slot=u'article_content')
            """@type: cms.models.placeholdermodel.Placeholder"""
            plugin = placeholder.cmsplugin_set \
                .filter(plugin_type='TextPlugin').first()
            """@type: cms.models.pluginmodel.CMSPlugin"""
            if plugin is not None:
                plugin_instance, plugin_class = plugin.get_plugin_instance()
                page_data['text'] = plugin_instance.body
            child_pages.append(page_data)
        context['child_pages'] = child_pages
        return context

plugin_pool.register_plugin(ChildArticlesPlugin)
