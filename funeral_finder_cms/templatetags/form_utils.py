# coding=utf-8
__author__ = 'Rinat'

import re
import django.template as d_t
import logging

register = d_t.Library()
logger = logging.getLogger(__name__)

ATTRIBUTE_RE = re.compile(r"""(?P<attr>[\w_-]+)=(?P<value>['"]?[^"']*['"]?)""", re.VERBOSE | re.UNICODE)


@register.tag
def render_label(parser, token):
    """
    Render a label of form field using given attribute-value pairs
    Takes form field as first argument and list of attribute-value pairs for
    all other arguments. Attribute-value pairs should be in the form of
    attribute=value or attribute="a value" for assignment.
    @param parser: template parser
    @type parser: django.template.base.Parser
    @param token: template token
    @type token: django.template.base.Token
    @return: template node
    @rtype: django.template.Node
    """
    error_msg = '{!r} tag requires a form field followed by a list of  attributes and ' \
                'values in the form attr="value"'.format(token.split_contents()[0])
    try:
        bits = token.split_contents()
        tag_name = bits[0]
        form_field = bits[1]
        attr_list = bits[2:]
    except ValueError:
        raise d_t.TemplateSyntaxError(error_msg)
    # assert False
    form_field = parser.compile_filter(form_field)
    attrs = []
    for pair in attr_list:
        match = ATTRIBUTE_RE.match(pair)
        if not match:
            raise d_t.TemplateSyntaxError(error_msg + ": %s" % pair)
        dct = match.groupdict()
        attrs.append((dct['attr'], parser.compile_filter(dct['value'])))
    # logger.debug('attrs: %r', attrs)
    return FieldLabelAttributeNode(form_field, attrs)


class FieldLabelAttributeNode(d_t.Node):
    def __init__(self, field, attrs):
        self.field = field
        self.attrs = attrs

    def render(self, context):
        bounded_field = self.field.resolve(context)
        attrs = {k: v.resolve(context) for k, v in self.attrs}
        return bounded_field.label_tag(attrs=attrs)
