# coding=utf-8
import django.http as d_h
import django.views.generic as d_v
import django.core.urlresolvers as d_urls
import django.contrib.auth as d_a
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator
from .forms import EmailAuthenticateForm


class BaseXhrView(d_v.View):
    http_method_names = ('get', 'post',)

    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            return d_h.HttpResponseForbidden(b'Ajax request only.')
        action_name = kwargs['action']
        if action_name.startswith('_'):
            return d_h.HttpResponseBadRequest(b'Action not knowing.')
        action = getattr(self, action_name.lower(), None)
        if action is None:
            return d_h.HttpResponseNotFound(b'Action not found.')
        return super(BaseXhrView, self).dispatch(request, action)

    def get(self, request, action):
        """
        Http GET request hanler
        @param request: http request object
        @type request: django.http.request.HttpRequest
        @param action: action method object
        @type action: function
        @return: http response object
        @rtype: django.http.response.HttpResponse
        """
        return action(params=request.GET)

    def post(self, request, action):
        """
        Http POST request hanler
        @param request: http request object
        @type request: django.http.request.HttpRequest
        @param action: action method object
        @type action: function
        @return: http response object
        @rtype: django.http.response.HttpResponse
        """
        return action(params=request.POST)


class XhrView(BaseXhrView):

    def set_coordinates(self, params=None):
        """
        Http POST request hanler
        @param request: http request object
        @type request: django.http.request.HttpRequest
        @param params: parmeters from request
        @type params: django.http.request.QueryDict
        @return: http response object
        @rtype: django.http.response.HttpResponse
        """
        self.request.session['geo'] = {
            'latitude': params.get('lat'),
            'longitude': params.get('lng')
        }
        return d_h.JsonResponse({
            'status': u'ok',
            'succes': u'Geo coordinates saved!'
        })

    def login(self, params=None):
        ret_data = {
            'status': u'error',
            'errors': None,
            'next': ''}
        form = EmailAuthenticateForm(self.request, data=params)
        if form.is_valid():
            user = form.get_user()
            ret_data['status'] = u'ok'
            if user.is_superuser:
                ret_data['next'] = d_urls.reverse('admin:index')
            else:
                ret_data['next'] = d_urls.reverse('profile_firms')
            d_a.login(self.request, user)
        else:
            ret_data['errors'] = form.errors
        return d_h.JsonResponse(ret_data)


def account_activate(request, uidb64=None, token=None):
    UserModel = d_a.get_user_model()
    assert uidb64 is not None and token is not None  # checked by URLconf
    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserModel._default_manager.get(pk=uid, is_active=False)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None
    else:
        user.is_active = True
        user.save()
        # d_a.login(request, user)
    if user is not None and default_token_generator.check_token(user, token):
        main_page_url = request.build_absolute_uri(location='/')
        return d_h.HttpResponse(
            content='<!doctype html>'
                    '<meta charset="utf-8"/>'
                    '<meta http-equiv="refresh" content="3; url=%s" />'
                    '<title>User is activated</title>'
                    '<body><h1>User is activated!</h1><p>Now you will be redirected to the main page.</p></body>'
                    % main_page_url)
    else:
        raise d_h.Http404

