/**
 * Created by Rinat on 09.08.2014.
 */

var FuneralFinderNS = FuneralFinderNS || {};
FuneralFinderNS.utils = FuneralFinderNS.utils || {};
FuneralFinderNS.handlers = FuneralFinderNS.handlers || {};

(function (w) {
    "use strict";

    var ns = FuneralFinderNS, h = ns.handlers;

    ns.firstClick = true;

    ns.extend(ns.handlers, {

        /**
         * Handler of click on button 'More information'
         * @param {MouseEvent} evt - object of event
         */
        showMore: function (evt) {
            var u = FuneralFinderNS.utils, newUrl = '';
            /** @type {Element} */
            /*
             if (ns.firstClick) {
             ns.firstClick = false;
             u.closestByClassName(evt.target, 'search').classList.add('anim');
             }
             */
            var searchItem = u.closestByClassName(evt.target, 'search-item');
            !searchItem.classList.contains('anim') && searchItem.classList.add('anim');
            var info = searchItem.querySelector('.roll');
            if (!info.classList.contains('open')) {
                info.classList.add('open');
                newUrl += location.pathname + location.search + '#' + searchItem.id;
                history.replaceState !== undefined && history.replaceState({}, '', newUrl);
            } else {
                info.classList.remove('open');
            }
            evt.preventDefault();
        },

        detailLink: function (evt) {
            var u = FuneralFinderNS.utils, newUrl = '';
            var searchItem = u.closestByClassName(evt.target, 'search-item');
            newUrl += location.pathname + location.search + '#' + searchItem.id;
            history.replaceState !== undefined && history.replaceState({}, '', newUrl);
        },

        enableAnim: function () {
            this.classList.remove('no-anim');
            this.removeEventListener('click', h.enableAnim);
        }

    });

    ns.body = document.body;
    ns.timer = null;

    window.addEventListener('scroll', function () {
        clearTimeout(ns.timer);
        if (ns.body.style.pointerEvents !== 'none') {
            ns.body.style.pointerEvents = 'none';
        }
        ns.timer = setTimeout(function () {
            ns.body.style.pointerEvents = '';
        }, 500);
    }, false);

    ns.onReady(function () {
        'use strict';

        var ns = FuneralFinderNS, h = ns.handlers, u = ns.utils;
        var $$ = document.querySelectorAll.bind(document);

        u.bindEventListener('click', $$('.search-item .show-info'), h.showMore);
        u.bindEventListener('click', $$('.search-item .detail-link > a'), h.detailLink);

        if (location.hash) {
            ns.winScrollTo = location.hash;
            history.pushState({}, document.title, location.pathname + location.search);
            ns.currSearchItem = document.querySelector(ns.winScrollTo);
            if (ns.currSearchItem !== null) {
                setTimeout(function () {
                    smoothScroll.animateScroll(null, ns.winScrollTo, {speed: 2000, easing: 'easeOutQuad', updateURL: false});
                    setTimeout(function () {
                        FuneralFinderNS.firstClick = false;
                        //document.querySelector('.search').classList.add('anim');
                        ns.currSearchItem.classList.add('anim');
                        ns.currSearchItem.querySelector('.roll').classList.add('open');
                        delete ns.currSearchItem;
                    }, 2500);

                }, 1000);
            }
        }

        // document.querySelector('.no-anim').addEventListener('click', h.enableAnim, false);

    });

})(window);
