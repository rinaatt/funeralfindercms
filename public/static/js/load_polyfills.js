/**
 * Created by Rinat on 25.08.2014.
 */
var FuneralFinderNS = FuneralFinderNS || {};

(function () {
    'use strict';

    /**
     * bind handler to event of document loaded
     * @param {Function} handler -- event handler
     */
    function bindReady (handler) {

        var called = false;

        function ready() {
            if (called) return;
            called = true;
            handler();
        }

        if (document.addEventListener) {
            document.addEventListener('DOMContentLoaded', function () {
                ready()
            }, false);
        } else if (document.attachEvent) {
            if (document.documentElement.doScroll && window == window.top) {
                var tryScroll = function () {
                    if (called) return;
                    if ( ! document.body) return;
                    try {
                        document.documentElement.doScroll('left');
                        ready();
                    } catch (e) {
                        setTimeout(tryScroll, 0);
                    }
                };
                tryScroll();
            }
            document.attachEvent('onreadystatechange', function () {
                if (document.readyState === 'complete') {
                    ready();
                }
            })
        }

        if (window.addEventListener)
            window.addEventListener('load', ready, false);
        else if (window.attachEvent)
            window.attachEvent('onload', ready);
    }

    var readyList = [];

    this.onReady = function (handler) {
        if ( ! readyList.length) {
            bindReady(function () {
                for (var i = 0; i < readyList.length; i++) {
                    readyList[i]();
                }
            })
        }
        readyList.push(handler)
    };

    var STATIC_URL = this.urls.STATIC_URL;
    window.Modernizr.load([
        {
            test: window.addEventListener,
            nope: STATIC_URL + 'js/eventlistener-polyfill.js'
        }, {
            test: Modernizr['classlist'],
            nope: STATIC_URL + 'js/classList.min.js'
        }, {
            test: Modernizr['dataset'],
            nope: STATIC_URL + 'js/html5-dataset.js'
        }, {
            test: Modernizr['history'],
            nope: STATIC_URL + 'js/history.min.js'
        }, {
            test: Modernizr['input']['placeholder'],
            nope: STATIC_URL + 'js/placeholders.min.js'
        }
    ]);

    /*
     * Polyfill Function.prototype.bind support for otherwise ECMA Script 5 compliant browsers
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind#Compatibility
     */
    if ( ! Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") {
                // closest thing possible to the ECMAScript 5
                // internal IsCallable function
                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
            }
            var aArgs = Array.prototype.slice.call(arguments, 1);
            var fToBind = this;
            var fNOP = function () {};
            var fBound = function () {
                return fToBind.apply(this instanceof fNOP && oThis ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
            };
            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();
            return fBound;
        };
    }

}).call(FuneralFinderNS);
