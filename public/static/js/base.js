/**
 * Created by Rinat on 09.08.2014.
 */
FuneralFinderNS = window.FuneralFinderNS || {};
FuneralFinderNS.handlers = FuneralFinderNS.handlers || {};
FuneralFinderNS.utils = FuneralFinderNS.utils || {};
FuneralFinderNS.cookie = FuneralFinderNS.cookie || {};
FuneralFinderNS.elements = FuneralFinderNS.elements || {};


(function (w) {
    'use strict';

    this.extend = function (trg, src, ret) {
        ret = ret || false;
        var _key;
        for (_key in src) if (src.hasOwnProperty(_key)) {
            trg[_key] = src[_key];
        }
        if (ret) {
            return trg;
        }
    };

    /*! based on code from github.com/toddmotto/atomic | (c) 2014 @toddmotto */
    this.ajax = (function () {

        'use strict';
        /**
         * Parse response text
         * @param {XMLHttpRequest} req
         * @returns {object|String}
         */
        function parse(req) {
            var result;
            try {
                result = JSON.parse(req.responseText);
            } catch (e) {
                result = req.responseText;
            }
            return result;
        }

        function xhr(type, url, data) {
            var hReq = null;
            var methods = {
                'success': function () { },
                'error': function () { }
            };
            if (w.XMLHttpRequest) {
                hReq = new XMLHttpRequest();
                if (hReq.overrideMimeType) {
                    hReq.overrideMimeType('text/xml');
                }
            } else if (w.ActiveXObject) { // IE6, IE5
                try {
                    hReq = new ActiveXObject('MSXML2.XMLHTTP.3.0');
                } catch (e) {
                    try {
                        hReq = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (e) {}
                }
            }
            hReq.open(type, url, true);
            hReq.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            hReq.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            /** @type {XMLHttpRequestProgressEvent} */
            hReq.onload = function (evt) {
                /** @type {XMLHttpRequest} */
                var r = evt.target;
                if (r.status === 200) {
                    var data = parse(r);
                    data['status'] === 'ok' && methods.success.apply(methods, [data, r]);
                    data['status'] === 'error' && methods.error.apply(methods, [data, r]);
                } else {
                    console.log(evt);
                }
            };
            hReq.send(data);
            return {
                'success': function (callback) {
                    methods.success = callback;
                    return methods;
                },
                'error': function (callback) {
                    methods.error = callback;
                    return methods;
                }
            };
        }

        return {
            'get': function (src) {
                return xhr('GET', src);
            },

            'put': function (url, data) {
                return xhr('PUT', url, data);
            },

            'post': function (url, data) {
                return xhr('POST', url, data);
            },

            'delete': function (url) {
                return xhr('DELETE', url);
            }
        };

    })();

    this.extend(this.handlers, {

        backward: function (evt) {
            w.history.back();
            evt.preventDefault();
        },

        select: function (evt) {
            this.classList.remove('placeholder');
            this.removeEventListener('change', this.select);
        },

        showModal: function (evt) {
            var ns = FuneralFinderNS;
            var btn = evt.currentTarget, modalID = btn.dataset['modalId'];
            var modalWin = document.getElementById('modal-win');
            var currModalID = modalWin.dataset['modalId'];
            if (currModalID !== modalID || currModalID === undefined) {
                if (currModalID !== undefined) {
                    var oldVirtWin = document.getElementById(currModalID);
                    while (modalWin.firstChild) oldVirtWin.appendChild(modalWin.firstChild);
                }
                var virtWin = document.getElementById(modalID);
                while (virtWin.firstChild) modalWin.appendChild(virtWin.firstChild);
                modalWin.dataset['modalId'] = modalID;
                modalWin.dataset['formPrepared'] = 0;
                document.getElementById('close-win')
                        .addEventListener('click', ns.handlers.hideModal, false);
            }
            document.body.parentElement.classList.add('show-modal');
            evt.preventDefault();
        },

        hideModal: function (evt) {
            document.body.parentElement.classList.remove('show-modal');
            // closeWin.removeEventListener('click', hideModal);
            // overlay.removeEventListener('click', hideModal);
            evt.preventDefault();
        },

        /**
         * Handler for `change` event on `input` elements
         * @param {Event} evt -- event object
         */
        inputValChange: function (evt) {
            var ns = FuneralFinderNS;
            var enabled = true,
                u = ns.utils,
                e = ns.elements;
            u.forEach(
                e.modalWin.querySelectorAll('input'),
                function (elem, ind, arr) {
                    enabled = enabled && ((elem.type == 'checkbox') ? elem.checked : !!elem.value);
                }
            );
            e.modalWin.querySelector('*[type=submit]').disabled = !enabled;
        },

        /**
         * Handler for `submit` event on simple contact form
         * @param {Event} evt -- event object
         */
        simpleFormSubmit: function (evt) {
            var ns = FuneralFinderNS, u = ns.utils;
            var req = ns.ajax.post(ns.urls.xhrQueryContact, u.encodeForm(this));
            req.success(function (data, xhr) {
                location.assign(data['next']);
            });
            req.error(function (data, xhr) {
                console.info('Contact send error!');
                console.debug(data);
            });
            evt.preventDefault();
        },

        prepareForm: function (evt) {
            var ns = FuneralFinderNS;
            var button = this,
                u = ns.utils,
                h = ns.handlers,
                e = ns.elements;
            setTimeout(function () {
                var simpleContactForm = document.forms['simple_contact'];
                simpleContactForm['firm_id'].value = button.dataset['firmId'];
                // e.modalWin.querySelector('input[name=firm_id]').value =
                if (e.modalWin.dataset['formPrepared'] == 0) {
                    u.bindEventListener('change', e.modalWin.querySelectorAll('input'), h.inputValChange);
                    u.bindEventListener('submit', e.modalWin.querySelector('form'), h.simpleFormSubmit);
                    e.modalWin.dataset['formPrepared'] = 1;
                }
            }, 0);
        },

        showMenu: function(evt) {
            w.document.getElementById('nav-menu').classList.add('in');
            evt.preventDefault();
        },

        hideMenu: function (evt) {
            var ns = FuneralFinderNS;
            var tagName = evt.target.tagName.toLowerCase();
            var $button = ns.utils.closestByTagName(evt.target, 'button');
            if (tagName === 'ul' || ($button && $button.className === 'close')) {
                window.document.getElementById('nav-menu').classList.remove('in');
                evt.preventDefault();
            }
        },

        getPhone: function (evt) {
            var ns = FuneralFinderNS;
            var u = ns.utils;
            var t = evt.target;
            if (t.tagName === 'SPAN') {
                evt.stopPropagation();
                t = t.parentNode;
            }
            if (t.dataset.action === 'get_phone') {
                // console.log(t.dataset.firm);
                var req = ns.ajax.post(
                    ns.urls.xhrGetPhone,
                    u.encodeDataAsForm(t.dataset));
                req.success(function (data, xhr) {
                    t.parentElement.removeChild(t);
                    // location.assign(data['next']);
                    // console.debug(data);
                    var $phonePlace = document.getElementById('company-' + t.dataset.firm + '-phone');
                    // console.log($phonePlace);
                    $phonePlace.outerHTML = '<a href="tel:' + data['phone'] + '">' + data['display'] + '</a>';
                });
                req.error(function (data, xhr) {
                    console.info('Contact send error!');
                    console.debug(data);
                });
            }
            evt.preventDefault();
        }

    });

    this.utils = this.extend(this.utils, {

        /**
         * Remove childs from Node
         * @param {Node} node -- cleared node
         */
        emptyNode: function (node) {
            while (node.lastChild)
                node.removeChild(node.lastChild);
        },

        /**
         * Utilities function for binds events
         * @param {String} eventName
         * @param {Node|NodeList} arr
         * @param {Function} callback
         */
        bindEventListener: function (eventName, arr, callback) {
            if (arr instanceof NodeList) {
                var _i = arr.length;
                while (_i--) arr[_i].addEventListener(eventName, callback, false);
            } else {
                arr.addEventListener(eventName, callback, false);
            }
        },

        /**
         * Handle static node list from querySelectorAll with Array.forEach method
         * @param {NodeList} arr
         * @param {Function} callback
         * @returns {*}
         */
        forEach: function (arr, callback) {
            return Array.prototype.slice.call(arr).forEach(callback);
        },

        /**
         * Getting values of form fields
         * @param {Element} form
         * @returns {object}
         */
        getFormValues: function (form) {
            var formValues = {};
            this.forEach(form.querySelectorAll('input'), function (inp, ind, arr) {
                var value = inp.value;
                if (inp.type == 'checkbox' && !inp.checked) {
                    value = null;
                }
                formValues[inp.name] = value;
            });
            return formValues;
        },

        /**
         * Encode the properties of an object as if they were name/value pairs from
         * an HTML form, using application/x-www-form-urlencoded format
         * @param {object} data -- associative array
         * @returns {String}
         */
        encodeDataAsForm: function (data) {
            if (!data) return '';
            var pairs = [], _name, _val;
            // For each name, skip inherited, skip methods
            for (_name in data) if (data.hasOwnProperty(_name) && typeof data[_name] !== 'function') {
                _val = data[_name].toString();
                _name = encodeURIComponent(_name.replace(" ", "+"));
                _val = encodeURIComponent(_val.replace(" ", "+"));
                pairs.push(_name + "=" + _val);
            }
            return pairs.join('&');
        },

        encodeForm: function (form) {
            return this.encodeDataAsForm(this.getFormValues(form));
        },

        /**
         * Find first closest element by class name
         * @param {Node|Element|EventTarget} $el
         * @param {String} className
         * @return {Element}
         */
        closestByClassName: function ($el, className) {
            var $p;
            while ($el && $el !== document) {
                $p = $el.parentElement;
                if ($p.classList.contains(className)) {
                    return $p;
                } else {
                    $el = $p;
                }
            }
            return null;
        },
        /**
         * Find first closest element by class name
         * @param {Node|Element|EventTarget} $el
         * @param {String} tagName
         * @return {Element|null}
         */
        closestByTagName: function ($el, tagName) {
            var $p; tagName = tagName.toLocaleUpperCase();
            while ($el && $el !== document.body) {
                $p = $el.parentElement;
                if ($p.tagName === tagName) {
                    return $p;
                } else {
                    $el = $p;
                }
            }
            return null;
        }

    }, true);

    this.extend(this.cookie, {
        nameClearPatt: new RegExp('([\.$?*|{}\(\)\[\]\\\/\+^])', 'g'),
        // возвращает cookie если есть или undefined
        get: function (name) {
            var matches = document.cookie.match(new RegExp('(?:^|; )' + name.replace(this.nameClearPatt, '\\$1') + '=([^;]*)'));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        },
        // уcтанавливает cookie
        set: function (name, value, props) {
            props = props || {};
            var exp = props.expires;
            if (typeof exp == "number" && exp) {
                var d = new Date();
                d.setTime(d.getTime() + exp * 1000);
                exp = props.expires = d;
            }
            if (exp && exp.toUTCString) {
                props.expires = exp.toUTCString();
            }

            value = encodeURIComponent(value);
            var updatedCookie = name + "=" + value;
            for (var propName in props) if (props.hasOwnProperty(propName)) {
                updatedCookie += "; " + propName;
                var propValue = props[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }
            document.cookie = updatedCookie;
        },
        // удаляет cookie
        del: function (name) {
            this.set(name, null, {expires: -1});
        }
    });

    this.bindCloseModalWin = function () {
        var h = FuneralFinderNS.handlers;
        // document.getElementById('close-win').addEventListener('click', h.hideModal, false);
        var overlay = document.querySelector('.overlay');
        overlay.addEventListener('click', h.hideModal, false);
        document.querySelector('.win').addEventListener('click', function (evt) {
            evt.stopPropagation();
        }, false);
    };

}).call(FuneralFinderNS, window);


FuneralFinderNS.onReady(function () {

    var ns = FuneralFinderNS;
    var h = ns.handlers, u = ns.utils, _i = 0;
    var $$ = window.document.querySelectorAll.bind(document);
    var $ = window.document.querySelector.bind(document);

    if (ns.elements['modalWin'] === undefined) {
        ns.elements.modalWin = document.getElementById('modal-win');
    }

    u.bindEventListener('click', $$('a[href="' + ns.urls.login + '"]'), h.showModal);
    u.bindEventListener('click', $$('button.contact'), h.showModal);
    u.bindEventListener('click', $$('button.contact'), h.prepareForm);
    u.bindEventListener('click', $('#show-nav-menu'), h.showMenu);
    u.bindEventListener('click', $('#hide-nav-menu'), h.hideMenu);
    u.bindEventListener('click', $('#nav-menu'), h.hideMenu);
    u.bindEventListener('click', $$('[data-action="get_phone"]'), h.getPhone);

    var retButt = document.querySelector('.return');
    retButt !== null && retButt.addEventListener('click', h.backward, false);

    var selectElems = document.getElementsByTagName('select');
    _i = selectElems.length;
    while (_i--) selectElems[_i].addEventListener('change', h.select, false);

    var backwardButton = document.querySelector('button.backward');
    if (!!backwardButton) {
        backwardButton.addEventListener('click', function (evt) {
            history.back();
        }, false);
    }
    ns.bindCloseModalWin();

    if ( ! ns.user.is_authenticated) {
        var loginForm = document.querySelector('#login form');
        /*
        loginForm.addEventListener('submit', function (evt) {
            "use strict";
            console.log(evt);
            evt.preventDefault();
        }, false);
        */
        u.bindEventListener('click', loginForm.querySelectorAll('.btn'), function (evt) {
            // console.log('pass_remind = %s', this.dataset['pass_remind']);
            loginForm.elements['pass_remind'].value = this.dataset['pass_remind'];
            if (this.dataset['pass_remind'] === '1') {
                loginForm.elements['password'].value = '';
            }
        });

        !! loginForm && loginForm.addEventListener('submit', function (evt) {
            var helpElem = loginForm.querySelector('.help');
            helpElem.innerHTML = '';
            var req = ns.ajax.post(ns.urls.xhrLogin, u.encodeForm(this));
            req.success(function (data, xhr) {
                console.info('Login success!');
                location.assign(data['next']);
            });
            req.error(function (data, xhr) {
                var /** @type {String} */ _key,
                    /** @type {Object} */ errors = data['errors'];
                // console.info('Login error!');
                for (_key in errors) if (errors.hasOwnProperty(_key)) {
                    var fd = document.createDocumentFragment();
                    var ul = document.createElement('ul');
                    ul.className = 'errorlist';
                    fd.appendChild(ul);
                    /** @type {Array} */
                    var errorList = errors[_key];
                    var _i = errorList.length, _html = '';
                    while (_i--) {
                        _html = '<li>' + errorList[_i] + '</li>' + _html;
                    }
                    ul.innerHTML = _html;
                    if (_key == '__all__') {
                        helpElem.appendChild(fd);
                    // loginForm.insertBefore(fd, helpElem.nextSibling);
                    }
                }
                // console.debug(data);
            });
            evt.preventDefault();
        }, false);

//        !! loginForm && $('#forget-pass').addEventListener('click', function (evt) {
//            var req = ns.ajax.post(ns.urls.xhrRemindPassword, u.encodeForm(loginForm));
//            req.success(function (data, xhr) {
//                console.info('Mail send success');
//                alert('The e-mail to "'+data['user_mail']+'" sent sucessfull!');
//                document.querySelector('.overlay').click();
//            });
//            req.error(function (data, xhr) {
//                 var /** @type {String} */ _key,
//                    /** @type {Object} */ errors = data['errors'];
//                console.info('Login error!');
//                for (_key in errors) if (errors.hasOwnProperty(_key)) {
//                    var fd = document.createDocumentFragment();
//                    var ul = document.createElement('ul');
//                    ul.className = 'errorlist';
//                    fd.appendChild(ul);
//                    /** @type {Array} */
//                    var errorList = errors[_key], _i = errorList.length, _html = '';
//                    while (_i--) {
//                        _html = '<li>' + errorList[_i] + '</li>' + _html;
//                    }
//                    ul.innerHTML = _html;
//                    if (_key == '__all__') {
//                        var helpElem = loginForm.querySelector('.help');
//                        loginForm.insertBefore(fd, helpElem.nextSibling);
//                    }
//                }
//                console.debug(data);
//            });
//            evt.preventDefault();
//        }, false);
    }

    /*
     var $area = w.document.getElementById('search-article-form-show');
     $area.addEventListener('click', function (evt) {
     if (this.$li === undefined) {
     this.$li = ns.utils.closestByTagName($area, 'li');
     }
     if (this.$li === null) {
     return;
     }
     try {
     if (!this.$li.classList.contains('show-form')) {
     this.$li.classList.add('show-form');
     } else {
     this.$li.classList.remove('show-form');
     }
     } catch (e) {
     console.log(e);
     }
     if (this.$li.querySelector('form') !== null) {
     evt.preventDefault();
     this.$li.querySelector('form input').focus();
     }
     // evt.stopPropagation();
     // return false;
     }, false);
     */

});
