/**
 * Created by Rinat on 12.09.2014.
 */

var FuneralFinderNS = FuneralFinderNS || {};
FuneralFinderNS.geo = FuneralFinderNS.geo || {};
FuneralFinderNS.urls = FuneralFinderNS.urls || {};

(function (w) {
    var ns = w.FuneralFinderNS;
    var g = ns.geo, u = ns.utils;

    g.options = {
        enableHighAccuracy: false,
        maximumAge : 30000,
        timeout : 27000
    };
    g.success = function (position) {
        var c = position.coords;
        var r = new XMLHttpRequest();
        var sendData = u.encodeDataAsForm({
            lat: Number(c.latitude).toFixed(6),
            lng: Number(c.longitude).toFixed(6)
        });
        r.open('POST', ns.urls.xhrSetCoordinates, true);
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); // set request type
        r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // set Content-Type
        r.responseType = 'text';
        r.onload = function (evt) {
            // console.log(evt);
            if (this.status === 200) {
                if (this.getResponseHeader('Content-Type') == 'application/json') {
                    var data = JSON.parse(this.responseText);
                    console.log(data);
                }
            }
        };
        r.send(sendData);
    };
    g.failure = function (error) {
        alert([
            'Unable to retrieve your location due to',
            error.code, ':', error.message
        ].join(' '));
    };

    g.findMe = function () {
        var n = w.navigator;
        if (n.geolocation) {
            n.geolocation.getCurrentPosition(this.success, this.failure, this.options);
        } else {
            alert('Geolocation services are not supported by your web browser.');
        }
    };

    ns.onReady(g.findMe.bind(g));

})(window);
