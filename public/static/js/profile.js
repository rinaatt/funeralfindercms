/**
 * Created by Rinat on 24.10.2014.
 */

var FuneralFinderNS = FuneralFinderNS || {};
FuneralFinderNS.handlers = FuneralFinderNS.handlers || {};

(function (w) {
    "use strict";

    var ns = FuneralFinderNS;

    ns.extend(ns.handlers, {

        handleImage: function () {
            var r = document.getElementById('result');
            if (r.children.length) r.removeChild(r.children[0]);
            r.appendChild(this);
        },

        handleFiles: function (e) {
            var img = document.getElementById('logo-upload-img');
            //img.addEventListener('load', handleImage, false);
            img.src = w.URL.createObjectURL(this.files[0]);
        }
    });

    document.getElementById('id_logo')
        .addEventListener('change', ns.handlers.handleFiles, false);

})(window);
