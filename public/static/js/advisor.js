/**
 * Created by Rinat on 24.10.2014.
 */

var FuneralFinderNS = FuneralFinderNS || {};
FuneralFinderNS.handlers = FuneralFinderNS.handlers || {};

(function (w) {
    "use strict";

    var ns = FuneralFinderNS;
    var h = ns.handlers, u = ns.utils;
    var $ = w.document.querySelector.bind(w.document);
    var $body = $('body');
    var hideOverMenu = function (evt) {
        if (evt.target.id !== 'articles-menu') {
            $('#articles-menu').classList.remove('overmenu');
            $body.removeEventListener('click', this, false);
        }
    };
    u.bindEventListener('click', $('.left-menu ul > li.active a'), function (evt) {
        var tag = evt.target.tagName.toLowerCase();
        if (tag === 'a') {
            evt.preventDefault();
            evt.stopPropagation();
            $('#articles-menu').classList.add('overmenu');
            u.bindEventListener('click', $body, hideOverMenu);
        }
    });
})(window);
