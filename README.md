# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


Hi Rinat,

please find enclosed the feedback by the designer and the corrections, which need to be made as well:

Main page (desktop)

- in the intro text the ‚ö‘ isn’t shown should be included in the setting of the font. *
- the ‚Dienstleister’ login button should be fully clickable not only the text *
- the logo is clickable and returns to the starting page but the mouse pointer doesn’t change - which is common usually. *
- to adjust the background color to the responsive designs; the original contrast of light grey to white is probably to little. *
- the distance between Headline (…mit unserer Kompetenz) > feather > Text > Button should be all the same and the feather should be centered. * 
- the pop up ‚Dienstleister Login‘ needs more space and all the same over and under the buttons. The button style should be adjusted to the newer one on the starting page.

Catalogue page (desktop)

- the text headline is not left bound, this should be the case
- the menu folds out a little to abrupt, could it be done the ‚mehr Info‘ in the search results?

Main page (responsive design)  

- when the main navigation is opened the ‚x‘ should be at the same position like the menu icon before opening it.
- the closing of the ‚x‘ should be all the same, either the old or the new (compared. Dienstleister Login & responsive menu)
- make the full button clickable in the menu not only the text

Catalogue page (responsive design) 

- the distance from text to the edges is missing most of the time, that should be corrected.
- sub menu looks good but the text field should not ‚jump‘ up and down when we open or close

That’s all as far as the designer goes

Have a nice day!
Christian 
