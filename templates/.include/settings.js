var FuneralFinderNS = FuneralFinderNS || {};

(function () {
    'use strict';

    this.user = {
        'is_authenticated': !! {% if user.is_authenticated %}1{% else %}0{% endif %}
    };

    this.urls = {
        'xhrSetCoordinates': '{% url "xhr" action="set_coordinates" %}',
        'xhrQueryContact': '{% url "catalog:xhr" action="query_contact" %}',
        // 'xhrLeaveContact': '{% url "catalog:xhr" action="leave_contact_page" %}',
        'xhrGetPhone': '{% url "catalog:xhr" action="get_phone" %}',
        'xhrLogin': '{% url "xhr" action="login" %}',
        'xhrRemindPassword': '{% url "xhr" action="password_remind" %}',
        'STATIC_URL': '{{ STATIC_URL }}',
        'login': '{% url "auth:login" %}'
    };

}).call(FuneralFinderNS);
