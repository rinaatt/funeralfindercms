# encoding: UTF-8
# Require any additional compass plugins here.
require 'breakpoint'

# Set this to the root of your project when deployed:
project_path = "public/static/"
http_path = "/static/"
sass_dir = "sass/"
css_dir = "css/"
images_dir = "img/"
# images_path = project
javascripts_dir = "js/"
fonts_dir = "css/fonts/"
fonts_path = project_path + fonts_dir

sourcemap = true
# You can select your preferred output style here (can be overridden via the command line):
output_style = :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = false

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

