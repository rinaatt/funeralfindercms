# coding=utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _


__author__ = 'Rinat'


class ArticlesPlugin(CMSPluginBase):
    name = _('Blog Articles Plugin')
    # model = CMSPlugin
    render_template = "blog/cms_plugins/articles.html"

    # def render(self, context, instance, placeholder):
    #     context['form'] = SearchForm()
    #     return context


# plugin_pool.register_plugin(ArticlesPlugin)
