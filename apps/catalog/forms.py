# coding=utf-8
import django.forms as d_f
from django.forms.models import modelformset_factory
from django.utils.translation import ugettext_lazy as _

from djangocms_text_ckeditor.widgets import TextEditorWidget

from . import models as m


class SearchForm(d_f.Form):
    # SERVICE_CHOICES = (
    #     (0, _(u'Show all')),
    #     (1, _(u'Funeral services')),
    #     (2, _(u'Funeral assistance and accompaniment')),
    #     (3, _(u'Funeral music')),  # Trauermusik
    #     (4, _(u'Sadness and grave speakers')),
    #     (5, _(u'Carpenters and Joiners')),
    #     (6, _(u'Cemetery gardener')),
    #     (7, _(u'Grave decorations and funeral flowers')),
    # )
    # # service = d_f.ChoiceField(choices=SERVICE_CHOICES, label=_(u'Service'), initial=1)
    query = d_f.CharField(max_length=100, min_length=1, label=u'Bestatter finden', label_suffix=u'...',
                          help_text=u'Bitte Ort oder Postleitzahl eingeben')


class ProfileForm(d_f.ModelForm):
    class Meta:
        model = m.Company
        fields = (
            'logo', 'title', 'street_house', 'postal_code',
            'city', 'phone', 'mail', 'website', 'description',
        )
        labels = {
            'logo': u'Logo upload'
        }
        # widgets = {
        #     'description': TextEditorWidget()
        # }


class PriceForm(d_f.ModelForm):
    class Meta:
        model = m.Price
        fields = ('id', 'active', 'title', 'summa', )
        labels = {
            'active': u' '
        }

    def __init__(self, **kwargs):
        kwargs.update(label_suffix='')
        super(PriceForm, self).__init__(**kwargs)


PriceFormSet = modelformset_factory(m.Price, form=PriceForm, min_num=2, max_num=2, extra=2)


class _ContactForm(d_f.Form):
    name = d_f.CharField(max_length=200, min_length=1, label=u'Name und Vorname *')
    mail = d_f.EmailField(label=u'E-mail adresse *')


class SimpleContactForm(_ContactForm):
    is_agree = d_f.BooleanField(label=u'* Ich bin mit der Weitergabe meiner Email-Adresse an bei „Bestattungshilfe“'
                                      u' registrierte Firmen einverstanden.')
    firm_id = d_f.IntegerField(widget=d_f.HiddenInput)
    # last_query = d_f.CharField(widget=d_f.HiddenInput)

    def clean_is_agree(self):
        _val = self.cleaned_data['is_agree']
        if not _val:
            raise d_f.ValidationError(u'You don\'t agree with rules!')
        return _val

    def clean(self):
        cleaned_data = super(SimpleContactForm, self).clean()
        _id = cleaned_data.get('firm_id', None)
        if not _id:
            raise d_f.ValidationError(u'Firm id is empty!')
        elif not m.Company.companies.filter(pk=_id).exists():
            raise d_f.ValidationError(u'Firm does not exists!')


class FullContactForm(_ContactForm):
    address = d_f.CharField(max_length=200, label=u'Anschrift *')
    phone = d_f.CharField(max_length=20, label=u'Telefon', required=False,
                          widget=d_f.TextInput({'type': 'tel'}))
    message = d_f.CharField(max_length=2000, label=u'Nachricht an den Anbieter *', widget=d_f.Textarea)
