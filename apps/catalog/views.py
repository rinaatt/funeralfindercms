# coding=utf-8
import re
import urllib

import django.http as d_h
import django.views.generic as d_v
from django.utils.translation import ugettext as _
# import django.core.mail as d_mail
# import django.core.signing as d_sign
from django.core.paginator import InvalidPage
import django.shortcuts as d_sc
# from django.conf import settings as d_st
# from django.contrib.auth import authenticate
from django.views.generic.edit import BaseUpdateView, ModelFormMixin
from funeral_finder_cms.paginator import CustomPaginator
from . import models as m
from . import forms as f
from funeral_finder_cms.views import BaseXhrView
from django.http.request import QueryDict
# import django.contrib.auth as d_a
# import django.core.urlresolvers as d_urls
from . import tasks as tsk
import logging
log = logging.getLogger('funeral_finder_cms')


not_dig_patt = re.compile(r'\D+')


def format_phone(num):
    num = not_dig_patt.sub(u'', num)
    if len(num) == 11:
        disp = num[0:3] + ' ' + num[3:7] + ' ' + num[7:]
    else:
        disp = num
    return num, disp


class XhrView(BaseXhrView):

    def query_contact(self, params=None):
        ret_data = {
            'status': u'error',
            'errors': None,
            'next': ''}
        form = f.SimpleContactForm(data=params)
        name = None
        mail = None
        if form.is_valid():
            cd = form.cleaned_data
            name = urllib.unquote_plus(form.cleaned_data['name'])
            mail = urllib.unquote_plus(form.cleaned_data['mail'])
            sess = self.request.session
            """@type: django.contrib.sessions.base.SessionBase"""
            sess['contact'] = {'name': name, 'mail': mail}
            firm = m.Company.companies.get(pk=cd['firm_id'])
            """@type: Company"""
            ret_data['status'] = u'ok'
            ret_data['next'] = firm.get_contact_url()
            sess['last_firm_id'] = firm.id
        else:
            ret_data['errors'] = form.errors
        resp = d_h.JsonResponse(ret_data)
        if name and mail:
            resp.set_signed_cookie('contact_name', name, httponly=True)
            resp.set_signed_cookie('contact_mail', mail, httponly=True)
        return resp

    def get_phone(self, params=None):
        # log.debug(u'PARAMS: %r' % params)
        ret_data = {'status': u'error', 'errors': None}
        if isinstance(params, QueryDict):
            firm = params.get('firm')
            """@type: unicode"""
            if firm.isdigit():
                company_id = int(firm)
                try:
                    company = m.Company.companies.get(pk=company_id)
                except m.Company.DoesNotExist:
                    ret_data.update(errors=[u'Company does not exist'])
                else:
                    # import random
                    # num_int = random.randint(1, 99)
                    # num = format(num_int, u'0=#2')
                    num, disp = format_phone(company.phone)
                    ret_data = {
                        'status': u'ok',
                        # 'phone': u'030123456%s' % num,
                        'phone': num,
                        # 'display': u'030 123456-%s' % num
                        'display': disp,
                    }
        else:
            ret_data.update(errors=[u'No params', ])
        return d_h.JsonResponse(ret_data)


class GetContactMixin(object):
    request = None
    """@type: django.http.request.HttpRequest"""
    mail = None
    name = None
    is_gave_contact = False

    def get_contact(self):
        contact = self.request.session.get('contact', None)
        if contact:
            self.name = contact['name']
            self.mail = contact['mail']
            self.is_gave_contact = True
        else:
            self.name = self.request.get_signed_cookie('contact_name', '')
            self.mail = self.request.get_signed_cookie('contact_mail', '')
            if self.name and self.mail:
                self.is_gave_contact = True
                self.request.session['contact'] = {
                    'name': self.name,
                    'mail': self.mail
                }


class IndexView(d_v.TemplateView, GetContactMixin):
    http_method_names = ('get', 'head', )
    template_name = 'catalog/index.html'

    def get_context_data(self, **kwargs):
        kwargs.update(form=f.SearchForm())
        return super(IndexView, self).get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        """
        Http get method
        @param request: http request
        @type request: django.http.request.HttpRequest
        @return: http response
        @rtype: django.http.response.HttpResponse
        """
        self.get_contact()
        return super(IndexView, self).get(request, *args, **kwargs)


class GeneralSearchView(d_v.ListView, GetContactMixin):
    http_method_names = ('get', )
    template_name = 'catalog/search_details.html'
    list_template_name = 'catalog/.include/items_list.html'
    allow_empty = True
    paginate_by_kwarg = 'paginate_by'
    paginate_by_nums = (10, 20, 50, 100, )
    paginate_by = 10
    paginator_class = CustomPaginator
    context_object_name = 'firms'
    model = m.Company
    uri_query = None
    page_patt = re.compile(r'page=\d+')

    def get_paginate_by(self, queryset):
        """
        Get size of pagination
        @param queryset: queryset object
        @type queryset: django.db.models.QuerySet
        @return: number
        @rtype: int
        """
        req = self.request
        """@type: django.http.request.HttpRequest"""
        sess = getattr(req, 'session')
        """@type: django.contrib.sessions.backends.base.SessionBase"""
        paginate_by_kwarg = self.paginate_by_kwarg
        paginate_by = self.kwargs.get(paginate_by_kwarg) or req.GET.get(paginate_by_kwarg)
        if paginate_by is None:
            paginate_by = sess.get(paginate_by_kwarg, self.paginate_by)
        else:
            sess[paginate_by_kwarg] = paginate_by
        self.paginate_by = paginate_by
        if queryset.query.high_mark is None:
            return paginate_by
        return None

    def paginate_queryset(self, queryset, page_size):
        """
        Paginate the queryset, if needed.
        """
        paginator = self.get_paginator(
            queryset, page_size, orphans=self.get_paginate_orphans(),
            allow_empty_first_page=self.get_allow_empty())
        page_kwarg = self.page_kwarg
        page = self.kwargs.get(page_kwarg) or self.request.GET.get(page_kwarg) or 1
        try:
            page_number = int(page)
        except ValueError:
            if page == 'last':
                page_number = paginator.num_pages
            else:
                raise d_h.Http404(_("Page is not 'last', nor can it be converted to an int."))
        page = paginator.page(page_number)
        return paginator, page, page.object_list, page.has_other_pages()

    def get_queryset(self):
        qs = super(GeneralSearchView, self).get_queryset()
        """@type: CompaniesQuerySet"""
        # return qs.prefetch_related('price_list').with_mail()
        return qs.prefetch_related('price_list')

    def get(self, request, *args, **kwargs):
        """
        Http GET request hanler
        @param request: http request object
        @type request: django.http.request.HttpRequest
        @return: http response object
        @rtype: django.http.response.HttpResponse
        """
        self.get_contact()
        req_get = request.GET
        """@type: django.http.request.QueryDict"""
        if 'page' in req_get:
            get_params = req_get.copy()
            del get_params['count']
            del get_params['page']
            self.uri_query = get_params.urlencode()
        else:
            self.uri_query = req_get.urlencode()
        try:
            return super(GeneralSearchView, self).get(request, *args, **kwargs)
        except InvalidPage:
            return d_h.HttpResponseRedirect(
                self.page_patt.sub('page=1', request.get_full_path()))


class SearchByFormView(GeneralSearchView):
    srch_patt = re.compile(r'(\d{5})\W+(\w+)', re.U | re.I)
    form = None
    list_mode = 'form_search'

    def get_context_data(self, **kwargs):
        kwargs.update(
            form=self.form,
            contact_form=f.SimpleContactForm())
        return super(SearchByFormView, self).get_context_data(**kwargs)

    def get_queryset(self):
        str_query = self.form.cleaned_data['query']
        """@type: unicode"""
        srch_res = self.srch_patt.match(str_query)
        qs = super(SearchByFormView, self).get_queryset()
        if srch_res:
            qs = qs.by_code(srch_res.group(1), srch_res.group(2))
        elif str_query.isdecimal():
            qs = qs.by_code(str_query)
        else:
            qs = qs.by_place(str_query)
        if qs is not None:
            return qs
        return m.Company.companies.none()

    def get(self, request, *args, **kwargs):
        """
        Http GET request hanler
        @param request: http request object
        @type request: django.http.request.HttpRequest
        @return: http response object
        @rtype: django.http.response.HttpResponse
        """
        self.form = f.SearchForm(request.GET)
        if not self.form.is_valid():
            return d_sc.redirect('pages-root')
        request.session['search_url'] = request.get_full_path()
        request.session['last_query'] = self.form.cleaned_data['query']
        return super(SearchByFormView, self).get(request, *args, **kwargs)


class SearchByCharView(GeneralSearchView):
    list_mode = 'char_search'

    def get_context_data(self, **kwargs):
        kwargs.update(
            char=self.kwargs['char'],
            contact_form=f.SimpleContactForm())
        return super(SearchByCharView, self).get_context_data(**kwargs)

    def get_queryset(self):
        qs = super(SearchByCharView, self).get_queryset()
        return qs.filter(title__startswith=self.kwargs['char'])


class DetailView(d_v.DetailView):
    http_method_names = ('get', 'head', )
    template_name = 'catalog/detail.html'
    uid_url_kwarg = 'uid'
    context_object_name = 'company'

    def get_context_data(self, **kwargs):
        kwargs.update(contact_form=f.SimpleContactForm())
        return super(DetailView, self).get_context_data(**kwargs)

    def get_queryset(self):
        uid = self.kwargs.get(self.uid_url_kwarg, None)
        return m.Company.companies.filter(uid=uid)


class SaveSimpleUserContactView(d_v.FormView):
    http_method_names = ('post', )
    form_class = f.SimpleContactForm

    def form_valid(self, form):
        """
        If the form is valid, redirect to the supplied URL.
        @param form: django form
        @type form: django.forms.forms.BaseForms
        """
        cd = form.cleaned_data
        sess = self.request.session
        """@type: django.contrib.sessions.base.SessionBase"""
        sess['contact'] = {
            'name': cd['name'],
            'mail': cd['mail']
        }
        try:
            firm = m.Company.companies.get(pk=cd['firm_id'])
            """@type: Undertaker"""
        except m.Company.DoesNotExist:
            raise d_h.Http404()
        resp = d_h.HttpResponseRedirect(firm.get_contact_url())
        resp.set_signed_cookie('contact_name', cd['name'], httponly=True)
        resp.set_signed_cookie('contact_mail', cd['mail'], httponly=True)
        return resp

    def get(self, request, *args, **kwargs):
        resp = d_h.HttpResponse()
        resp.status_code = 204
        return resp


class FirmContactView(d_v.FormView, GetContactMixin):
    form_class = f.FullContactForm
    template_name = 'catalog/contact.html'
    firm = None

    def _get_task_data(self):
        sess = self.request.session
        return {
            'user_name': self.name,
            'user_mail': self.mail,
            'org_title': self.firm.title,
            'org_mail': self.firm.mail,
            'org_postal_code': self.firm.postal_code,
            'last_query': sess.get('last_query', u''),
            'text': u'',
        }

    def get_initial(self):
        initial = super(FirmContactView, self).get_initial()
        initial.update({
            'name': self.name,
            'mail': self.mail
        })
        return initial

    def form_valid(self, form):
        sess = self.request.session
        task_data = self._get_task_data()
        task_data['user_name'] = form.cleaned_data['name']
        task_data['user_mail'] = form.cleaned_data['mail']
        task_data['text'] = form.cleaned_data['message']
        tsk.send_message.delay(task_data)
        return self.render_to_response(
            self.get_context_data(
                search_url=sess.get('search_url'), form_valid=True))

    def _set_firm(self, slug, uid):
        self.firm = d_sc.get_object_or_404(
            m.Company, slug=slug, uid=uid)

    def get(self, request, *args, **kwargs):
        # self.get_contact()
        # if self.is_gave_contact:
        sess = request.session
        self._set_firm(self.kwargs['slug'], self.kwargs['uid'])
        if sess.get('last_firm_id') != self.firm.id:
            # tsk.user_intresting.delay(self._get_task_data())
            sess['last_firm_id'] = self.firm.id
        return super(FirmContactView, self).get(request, *args, **kwargs)
        # return d_sc.redirect('catalog:detail', slug=kwargs['slug'], uid=kwargs['uid'])

    def post(self, request, *args, **kwargs):
        # self.get_contact()
        # if self.is_gave_contact:
        self._set_firm(self.kwargs['slug'], self.kwargs['uid'])
        return super(FirmContactView, self).post(request, *args, **kwargs)
        # return d_sc.redirect('pages-root')


class ProfileFirmsView(d_v.ListView):
    object_list = []
    template_name = 'catalog/search_details.html'
    list_template_name = 'catalog/.include/manager_items_list.html'
    paginate_by = 10
    paginator_class = CustomPaginator
    list_mode = 'manager'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return super(ProfileFirmsView, self).dispatch(request, *args, **kwargs)
        else:
            return d_sc.redirect('pages-root')

    def get_queryset(self):
        user = self.request.user
        qs = m.Company.companies.all()
        if not user.is_superuser:
            qs = qs.filter(user=user)
        return qs

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        if self.object_list.count() == 1:
            firm = self.object_list[0]
            return d_sc.redirect(firm.get_edit_url())
        context = self.get_context_data()
        return self.render_to_response(context)


class FirmEditView(d_v.UpdateView):
    form_class = f.ProfileForm
    template_name = 'catalog/profile.html'
    model = m.Company
    object = None
    """@type: m.Company"""
    context_object_name = 'firm'
    price_formset = None

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return super(FirmEditView, self).dispatch(request, *args, **kwargs)
        else:
            return d_sc.redirect('pages-root')

    def get_queryset(self):
        return self.model.companies.filter(user=self.request.user)

    def form_valid(self, form):
        self.object = form.save()
        price_formset = f.PriceFormSet(self.request.POST, prefix='p')
        if price_formset.is_valid():
            instances = price_formset.save(commit=False)
            """@type: list[apps.search.models.Price]"""
            for instance in instances:
                if instance.company is None:
                    instance.company = self.object
                    instance.save()
        return super(ModelFormMixin, self).form_valid(form)

    def get_context_data(self, **kwargs):
        if self.object:
            kwargs.update(
                price_formset=f.PriceFormSet(
                    queryset=self.object.price_list.all(),
                    prefix='p', initial=[
                        {'active': True, 'title': u'Erdbestattung'},
                        {'active': True, 'title': u'Feuerbestattung'}
                    ]
                )
            )
        return super(FirmEditView, self).get_context_data(**kwargs)
