# coding=utf-8
from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from . import views as v


urlpatterns = [
    url(r'^search/contact$', v.SaveSimpleUserContactView.as_view(), name='simple_contact'),
    url(r'^search$', v.SearchByFormView.as_view(), name='search'),
    url(r'^bestatter/(?P<char>\w)$', v.SearchByCharView.as_view(), name='search_char'),
    url(r'^xhr/(?P<action>\w+)$', csrf_exempt(v.XhrView.as_view()), name='xhr'),
    url(r'^(?P<slug>[\w\-]+)-(?P<uid>[0-9a-f]{32})/contact$', v.FirmContactView.as_view(), name='firm_contact'),
    url(r'^(?P<slug>[\w\-]+)-(?P<uid>[0-9a-f]{32})$', v.DetailView.as_view(), name='detail'),
    url(r'^$', v.IndexView.as_view(), name='index'),
]
