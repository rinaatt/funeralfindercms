# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20151006_0957'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='description',
            field=djangocms_text_ckeditor.fields.HTMLField(help_text='Free text with predefined boilerplate that can be changed but', null=True, verbose_name='description', blank=True),
        ),
    ]
