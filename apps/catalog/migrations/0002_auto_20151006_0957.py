# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='active',
            field=models.BooleanField(default=True, verbose_name='is active?'),
        ),
        migrations.AlterField(
            model_name='price',
            name='company',
            field=models.ForeignKey(related_name='price_list', editable=False, to='catalog.Company', null=True, verbose_name='company'),
        ),
        migrations.AlterField(
            model_name='price',
            name='summa',
            field=models.DecimalField(verbose_name='price from', max_digits=12, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='price',
            name='title',
            field=models.CharField(max_length=100, verbose_name='service type'),
        ),
    ]
