# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import catalog.models.company
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name='firm title')),
                ('region', models.CharField(max_length=150, null=True, verbose_name='region')),
                ('city', models.CharField(max_length=150, verbose_name='city', db_index=True)),
                ('postal_code', models.CharField(max_length=5, verbose_name='postal code', db_index=True)),
                ('street_house', models.CharField(max_length=150, verbose_name='street and house')),
                ('phone', models.CharField(max_length=100, null=True, verbose_name='phone', blank=True)),
                ('fax', models.CharField(max_length=100, null=True, verbose_name='fax', blank=True)),
                ('mail', models.EmailField(max_length=100, null=True, verbose_name='e-mail', blank=True)),
                ('website', models.URLField(null=True, verbose_name='website', blank=True)),
                ('description', models.TextField(help_text='Free text with predefined boilerplate that can be changed but', null=True, verbose_name='description', blank=True)),
                ('logo', models.ImageField(upload_to=catalog.models.company._make_upload_path, null=True, verbose_name='logo', blank=True)),
                ('slug', models.SlugField(max_length=200, editable=False)),
                ('created', models.DateField(auto_now_add=True, verbose_name='created')),
                ('updated', models.DateTimeField(verbose_name='updated', null=True, editable=False)),
                ('latitude', models.FloatField(verbose_name='latitude', null=True, editable=False)),
                ('longitude', models.FloatField(verbose_name='longitude', null=True, editable=False)),
                ('uid', models.CharField(verbose_name='Firm UID', max_length=32, null=True, editable=False, db_index=True)),
                ('user', models.ForeignKey(related_name='companies', verbose_name='manager', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'company',
                'managed': True,
                'verbose_name_plural': 'companies',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.CharField(max_length=2, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='GeoFeatureClass',
            fields=[
                ('id', models.CharField(max_length=1, serialize=False, verbose_name='class', primary_key=True)),
                ('desc', models.CharField(max_length=100, verbose_name='description')),
            ],
        ),
        migrations.CreateModel(
            name='GeoFeatureCode',
            fields=[
                ('id', models.CharField(max_length=10, serialize=False, verbose_name='code', primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('desc', models.CharField(max_length=200, null=True, verbose_name='description')),
                ('cls', models.ForeignKey(to='catalog.GeoFeatureClass')),
            ],
        ),
        migrations.CreateModel(
            name='GeoName',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, db_index=True)),
                ('latitude', models.FloatField(verbose_name='breite', editable=False)),
                ('longitude', models.FloatField(verbose_name='l\xe4nge', editable=False)),
                ('country', models.ForeignKey(to='catalog.Country')),
                ('feature_class', models.ForeignKey(to='catalog.GeoFeatureClass')),
                ('feature_code', models.ForeignKey(to='catalog.GeoFeatureCode')),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='InterestsLogging',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created', null=True)),
                ('user_name', models.CharField(max_length=200, verbose_name='user name')),
                ('user_mail', models.EmailField(max_length=254, verbose_name='user e-mail')),
                ('org_postal_code', models.CharField(max_length=5, verbose_name='postal code')),
                ('org_title', models.CharField(max_length=200, verbose_name='company title')),
                ('org_mail', models.EmailField(max_length=254, verbose_name='company e-mail')),
                ('event', models.CharField(max_length=100, verbose_name='event')),
                ('last_query', models.CharField(max_length=100, null=True, verbose_name='last search query', blank=True)),
                ('text', models.TextField(null=True, verbose_name='text', blank=True)),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.CreateModel(
            name='OtherContact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=100, verbose_name='contact value')),
                ('is_phone', models.BooleanField(default=False, db_index=True, verbose_name='is phone')),
                ('is_mobile', models.BooleanField(default=False, db_index=True, verbose_name='is mobile phone')),
                ('is_fax', models.BooleanField(default=False, db_index=True, verbose_name='is fax number')),
                ('is_email', models.BooleanField(default=False, db_index=True, verbose_name='is e-mail address')),
                ('company', models.ForeignKey(related_name='other_contacts', editable=False, to='catalog.Company', verbose_name='firm')),
            ],
            options={
                'verbose_name': 'Other contact',
                'verbose_name_plural': 'Other contacts',
            },
        ),
        migrations.CreateModel(
            name='PostalCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=20, db_index=True)),
                ('place_name', models.CharField(max_length=180)),
                ('latitude', models.FloatField(verbose_name='breite', editable=False)),
                ('longitude', models.FloatField(verbose_name='l\xe4nge', editable=False)),
                ('accuracy', models.SmallIntegerField(help_text='accuracy of lat/lng from 1=estimated to 6=centroid')),
                ('country', models.ForeignKey(to='catalog.Country')),
            ],
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('active', models.BooleanField(default=True, verbose_name='ist aktiv?')),
                ('title', models.CharField(max_length=100, verbose_name='service-typ')),
                ('summa', models.DecimalField(verbose_name='preis ab', max_digits=12, decimal_places=2)),
                ('company', models.ForeignKey(related_name='price_list', editable=False, to='catalog.Company', null=True, verbose_name='firm')),
            ],
            options={
                'verbose_name': 'Price',
                'verbose_name_plural': 'Prices',
            },
        ),
        migrations.AlterIndexTogether(
            name='postalcode',
            index_together=set([('code', 'place_name')]),
        ),
        migrations.AlterIndexTogether(
            name='geoname',
            index_together=set([('country', 'name', 'feature_code')]),
        ),
    ]
