# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20151122_2306'),
    ]

    operations = [
        migrations.RunSQL(
            "select setval('catalog_company_id_seq', (select max(id) from catalog_company), true)",
            reverse_sql="select setval('catalog_company_id_seq', 1, false)"),
    ]
