# coding=utf-8
__author__ = 'Rinat'

from django import template
register = template.Library()


# @register.simple_tag
# def get_verbose_field_name(value, arg):
#     return field_verbose_name(value, arg)


def search_space(s):
    import re
    i = 0
    mid = len(s) / 2
    res = None
    if re.search(r'\s', s) is not None:
        while True:
            res = mid - i
            if s[res] == u' ':
                break
            res = mid + i
            if s[res] == u' ':
                break
            i += 1
    return res


@register.filter
def split_title(value, min_length=15):
    value_len = len(value)
    if value_len > min_length:
        space_pos = search_space(value)
        if space_pos is not None and \
                (space_pos > 4 or space_pos < value_len - 4):
            val_1 = value[:space_pos]
            val_2 = value[space_pos+1:]
            value = u'%s<br/>%s' % (val_1, val_2, )
    return value

