# coding=utf-8
from .models import Company


def companies_alphabet(request):
    return {'FIRMS_ALPHABET': Company.companies.get_alphabet()}
