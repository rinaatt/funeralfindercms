# coding=utf-8
__author__ = 'Rinat'

from optparse import make_option
import os.path as op
import os
from datetime import date
import django.core.management as mnt
from django.db import connection, ProgrammingError
from django.utils.text import slugify
from django.conf import settings
import re

from searchfirms.models import Undertaker, Price, OtherContact

URL_API = 'http://open.mapquestapi.com/geocoding/v1/address'
BATCH_SIZE = 100

begin_digit_patt = re.compile(r'^\s*\d\.\s*')


class Command(mnt.BaseCommand):
    args = '<db tablename>'
    help = 'Loading data from database table'
    option_list = mnt.BaseCommand.option_list + (
        make_option('--no-backup', action='store_false',
                    dest='backup_table', default=True,
                    help='No backup old table data'),
    )
    _backup_dir = op.join(settings.PROJECT_ROOT, 'apps',
                          'searchfirms', 'fixtures', 'backup')

    def _backup_tables(self, *tables):
        if not op.exists(self._backup_dir):
            os.makedirs(self._backup_dir)
        file_path = op.join(
            self._backup_dir,
            date.today().strftime('%Y_%m_%d_searchfirms.xml'))
        if op.exists(file_path):
            self.stdout.write('Backup created yet! Skip...')
        else:
            with open(file_path, 'w') as fl:
                mnt.call_command(
                    'dumpdata', *tables, format='xml',
                    use_natural_foreign_keys=True, indent=2,
                    use_natural_primary_keys=True, stdout=fl)
            self.stdout.write('Successfull make backup')

    def handle(self, *args, **options):
        tab_name = args[0]  # 201408_funeralfinder
        batch_size = BATCH_SIZE
        if len(args) > 1:
            batch_size = args[1]
        if not tab_name:
            raise mnt.CommandError('No table name')
        cursor = connection.cursor()
        try:
            rownum = cursor.execute("SELECT 1 FROM `%s` LIMIT 1" % tab_name)
        except ProgrammingError:
            raise mnt.CommandError('Table `%s` doesn\'t exist!' % tab_name)
        else:
            self.stdout.write('Table `%s` is exists: %d' % (tab_name, rownum, ))
        if options['backup_table']:
            self._backup_tables('searchfirms.Price', 'searchfirms.Undertaker')
        else:
            self.stdout.write('Backup not created')
        Price.objects.all().delete()
        Undertaker.companies.all().delete()
        OtherContact.objects.all().delete()
        query = "SELECT " \
                "t1.Firma, t1.Bezirk, t1.Stadt, t1.PLZ, " \
                "concat_ws(' ', t1.Strasse, t1.Hausnummer) as addr, " \
                "t1.Telefon, t1.Fax, t1.Email, t1.homepage, t1.Firma_ID " \
                "FROM `%s` t1"
        rownum = cursor.execute(query % tab_name)
        objs = []
        contacts = []
        inserted_firms = []
        for row in cursor.fetchall():
            title = begin_digit_patt.sub('', row[0]).strip()
            firm_slug = slugify(title)
            firm_ = '{0}-{1}-{2}'.format(row[3], slugify(row[4]), firm_slug)
            if firm_ not in inserted_firms:
                inserted_firms.append(firm_)
                objs.append(Undertaker(
                    title=title, region=row[1], city=row[2], postal_code=row[3],
                    street_house=row[4], phone=row[5], fax=row[6], mail=row[7],
                    website=row[8], uid=row[9], slug=firm_slug))
            else:
                if len(objs):
                    Undertaker.companies.bulk_create(objs)
                    objs = []
                try:
                    company = Undertaker.companies.get(
                        slug=firm_slug, street_house=row[4], postal_code=row[3])
                    """@type: Undertaker"""
                except Undertaker.DoesNotExist:
                    self.stdout.write('No company with slug = %s, street_house = %s, postal_code = %s' %
                                      (firm_slug, row[4], row[3]))
                    raise
                if row[5]:
                    if not company.phone:
                        company.phone = row[5]
                    else:
                        contacts.append(OtherContact(
                            undertaker=company, value=row[5], is_phone=True))
                if row[6]:
                    if not company.fax:
                        company.fax = row[6]
                    else:
                        contacts.append(OtherContact(
                            undertaker=company, value=row[6], is_fax=True))
                if row[7]:
                    if not company.mail:
                        company.mail = row[7]
                    else:
                        contacts.append(OtherContact(
                            undertaker=company, value=row[7], is_email=True))
                company.save()
                # self.stdout.write(u'Add %d other contacts for %s' %
                # (len(contacts), company.title))
                if len(contacts) >= batch_size:
                    OtherContact.objects.bulk_create(contacts)
                    contacts = []
            if len(objs) >= batch_size:
                Undertaker.companies.bulk_create(objs)
                objs = []
        if len(objs):
            Undertaker.companies.bulk_create(objs)
        if len(contacts):
            OtherContact.objects.bulk_create(contacts)
        # cursor.execute("DROP TABLE `%s`" % tab_name)
        cursor.close()
        self.stdout.write('Successfully inserted %d rows of `%s`' % (rownum, tab_name))
