# coding=utf-8
__author__ = 'Rinat'

import simplejson as json
import urllib
import unicodedata

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from searchfirms.models import Undertaker

URL_API = 'http://open.mapquestapi.com/geocoding/v1/address'


class Command(BaseCommand):
    help = 'Filling latitude and longitude geo coordinates from open mapquest api'

    def handle(self, *args, **options):
        qs_firms = Undertaker.companies\
            .filter(Q(latitude=None) | Q(longitude=None))\
            .exclude(postal_code='', street_house='')
        for firm in qs_firms.order_by('id'):
            """@type firm: Undertaker"""
            if firm.street_house.strip().isdigit():
                continue
            addr_str = u','.join([firm.street_house, firm.postal_code, firm.city, u'Deutschland'])
            data = {
                'maxResults': 1,
                'thumbMaps': False,
                'ignoreLatLngInput': True,
                'key': 'Fmjtd|luur2hu2l9,bs=o5-9waxg4',
                'location': unicodedata.normalize('NFKC', addr_str).encode('utf-8')
            }
            url = URL_API + '?' + urllib.urlencode(data)
            try:
                res_dict = json.load(urllib.urlopen(url))
            except IOError:
                self.stdout.write('Firm ID = %d. Cannot open url: %s' %
                                  (firm.id, url))
                continue
            try:
                lat_lng = res_dict['results'][0]['locations'][0]['latLng']
            except IndexError:
                self.stdout.write('Firm ID = %d. No result with url: %s' %
                                  (firm.id, url))
                continue
            firm.latitude = lat_lng['lat']
            firm.longitude = lat_lng['lng']
            firm.save()
            self.stdout.write(u'Firm %s updated with lat = %f and lng = %f ' %
                              (firm.title, firm.latitude, firm.longitude))
        self.stdout.write('Success request geocoding data')
