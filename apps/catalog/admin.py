# coding=utf-8
# from django.db import models
from django.contrib import admin
# from django.contrib.flatpages.admin import FlatPageAdmin
# from django.contrib.flatpages.models import FlatPage
# from ckeditor.widgets import CKEditorWidget
# import searchfirms.models as m
# from seo_manager.admin import register_seo_admin
# from searchfirms.seo import FuneralMetadata

from .models import Company, InterestsLogging


@admin.register(InterestsLogging)
class InterestsLoggingAdmin(admin.ModelAdmin):
    actions = None
    date_hierarchy = 'created'
    exclude = ()
    list_display = (
        'created', 'event', 'org_postal_code',
        'org_title', 'org_mail',
        'user_name', 'user_mail',
        'last_query', )
    readonly_fields = ('created', )
    fieldsets = (
        (None, {
            'fields': (
                ('created', 'event'),
                ('org_postal_code', 'org_title', 'org_mail'),
                ('user_name', 'user_mail', 'last_query'),
                'text',
            ),
        }),
    )

    def has_add_permission(self, request):
        return False


@admin.register(Company)
class CompanyAdmim(admin.ModelAdmin):
    list_display = (
        'title', 'user',
        'street_house', 'postal_code',
        'city', 'mail', 'phone')
    search_fields = ('title', 'postal_code', 'city', 'mail', )


# class FlatPageCustom(FlatPageAdmin):
#     formfield_overrides = {
#         models.TextField: {'widget': CKEditorWidget}
#     }


# admin.site.unregister(FlatPage)
# admin.site.register(FlatPage, FlatPageCustom)
# admin.site.register(m.InterestsLogging, InterestsLoggingAdmin)
# admin.site.register(m.Undertaker, UndertakerAdmim)
# register_seo_admin(admin.site, FuneralMetadata)
