# coding=utf-8
from __future__ import absolute_import
import hashlib
import string
from manage_celery import app
import catalog.models as m
from django.conf import settings as d_st
from django.core import mail as d_mail
from django.contrib.auth.models import User

digs = string.digits + string.lowercase


def int2base(x, base):
    if not x:
        return '0'
    sign = -1 if x < 0 else 1
    x = abs(x)
    digits = []
    while x:
        digits.append(digs[x % base])
        x /= base
    if sign < 0:
        digits.append('-')
    digits.reverse()
    return ''.join(digits)


def hash_mail(email):
    digest = hashlib.md5(email).hexdigest()
    return int2base(int(digest, 16), len(digs))


@app.task(ignore_result=True)
def user_intresting(data):
    """
    Task for send mail to funeral
    @param data: parameters for task
    @type data: dict[str, str]
    """
    m.InterestsLogging.objects.create(event=u'user intresting', **data)
    org_mail = data['org_mail']
    if not User.objects.filter(email=org_mail).exists():
        org_passw = User.objects.make_random_password(length=8)
        new_user = User.objects.create_user(
            hash_mail(org_mail), email=org_mail, password=org_passw)
        m.Company.companies.filter(mail=org_mail).update(user=new_user)
        subj = u'Are interested in your services'
        msg = u'This dude is interested in your services.\n' \
              u'Your login and password: {0}/{1}'
        h_msg = u'<p>This dude is <b>interested</b> in your services.</p>' \
                u'<p>Your login and password: {0}/{1}</p>'
        from_email = d_st.EMAIL_ADDRESS_FROM
        recipients = [org_mail, ]
        if d_st.EMAIL_DEBUG:
            recipients = d_st.EMAIL_DEBUG_RECIPIENTS
        d_mail.send_mail(
            subj, msg.format(org_mail, org_passw),
            from_email, recipients,
            html_message=h_msg.format(org_mail, org_passw))


@app.task(ignore_result=True)
def send_message(data):
    """
    @param data: parameters for task
    @type data: dict[str, str]
    @return:
    @rtype:
    """
    m.InterestsLogging.objects.create(event=u'send message', **data)
    subj = u'You have new message from our user'
    msg = u'Text of message: {0}.\n You can answer it just click to <Answer>'.format(data['text'])
    h_msg = u'<h3>Text of message</h3><p>{0}</p>'.format(data['text'])
    email = d_mail.EmailMultiAlternatives(subj, msg)
    email.to.append(data['org_mail'])
    email.reply_to.append(data['user_mail'])
    email.attach_alternative(h_msg, 'text/html')
    email.send()
