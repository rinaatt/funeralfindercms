# coding=utf-8
import django.db.models as d_m
from django.utils.translation import ugettext_lazy as _
from .company import Company


class Price(d_m.Model):
    company = d_m.ForeignKey(
        Company, verbose_name=_(u'company'), related_name='price_list',
        editable=False, null=True)
    active = d_m.BooleanField(_(u'is active?'), default=True)
    title = d_m.CharField(_(u'service type'), max_length=100)
    summa = d_m.DecimalField(_(u'price from'), max_digits=12, decimal_places=2)

    class Meta:
        verbose_name = _(u'Price')
        verbose_name_plural = _(u'Prices')
