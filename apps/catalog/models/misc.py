# coding=utf-8
from django.utils.translation import ugettext_lazy as _
import django.db.models as d_m


class InterestsLogging(d_m.Model):
    created = d_m.DateTimeField(_(u'created'), auto_now_add=True, blank=True, null=True)  # erstellt
    user_name = d_m.CharField(_(u'user name'), max_length=200)  # benutzername
    user_mail = d_m.EmailField(_(u'user e-mail'))  # e-mail-benutzer
    org_postal_code = d_m.CharField(_(u'postal code'), max_length=5)  # firma plz
    org_title = d_m.CharField(_(u'company title'), max_length=200)  # firmenname
    org_mail = d_m.EmailField(_(u'company e-mail'))  # firma e-mail
    event = d_m.CharField(_(u'event'), max_length=100)  # veranstaltung
    last_query = d_m.CharField(_(u'last search query'), max_length=100, null=True, blank=True)
    text = d_m.TextField(_(u'text'), null=True, blank=True)

    class Meta:
        ordering = ('created',)
