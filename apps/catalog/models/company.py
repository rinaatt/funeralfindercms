# coding=utf-8
import urllib
import unicodedata
import datetime
import hashlib
import hmac
import base64

import django.db.models as d_m
import django.core.urlresolvers as d_u
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.utils.text import slugify
from django.contrib.auth.models import User
from djangocms_text_ckeditor.fields import HTMLField
from .postal_code import PostalCode
from .geo import GeoName


def _make_upload_path(inst, f_name):
    return u'logo/%d_%s' % (inst.id, f_name,)


class CompaniesQuerySet(d_m.QuerySet):

    def get_alphabet(self):
        # select distinct upper(substr(ltrim(title, '"-_'' '), 1, 1)) as letter
        # from catalog_company order by letter asc;
        return self.extra(select={
            'letter': "upper(substr(ltrim(title, '\" - _'' '), 1, 1))"})\
            .distinct().order_by('letter')\
            .values_list('letter', flat=True)

    def with_mail(self):
        return self.exclude(mail__isnull=True)

    def with_distance_to(self, lat, lng, precision=2):
        """
        Build query with evaluate distance to geo coordinate
        @param lat: latitude geo coordinate
        @type lat: float
        @param lng: longitude geo coordinate
        @type lat: float
        @return: querysyt
        @rtype: django.db.models.QuerySet
        """
        return self.filter(latitude__isnull=False, longitude__isnull=False) \
            .extra(
            select={
                'distance': "round(cast(acos("
                            "sin(%s * 0.0174532925) * sin(latitude * 0.0174532925) + "
                            "cos(%s * 0.0174532925) * cos(latitude * 0.0174532925) * "
                            "cos((%s - longitude)* 0.0174532925)) * 6370.6934856531 as numeric)"
                            ", %s)"
            },
            select_params=(lat, lat, lng, precision),
            order_by=('distance', 'title',)
        )

    def by_code(self, code, place=None):
        lat, lng = PostalCode.postal_codes \
            .get_coordinates(code, place)
        if lat is None:
            return None
        count = self.filter(postal_code=code).count()
        if count >= 10:
            return self.filter(postal_code=code) \
                .with_distance_to(lat, lng)
        if count > 0:
            return self.extra(
                select={'code_exact': 'postal_code = %s'},
                select_params=(code,),
                order_by=('code_exact',)
            ).with_distance_to(lat, lng)[:10]
        return self.with_distance_to(lat, lng)[:10]

    def by_place(self, name):
        lat, lng = GeoName.places.get_coordinates(name)
        if lat is None:
            return None
        count = self.filter(city=name).count()
        if count >= 10:
            return self.filter(city=name).with_distance_to(lat, lng)
        if count > 0:
            return self.extra(
                select={'city_exact': 'city = %s'},
                select_params=(name,),
                order_by=('city_exict',)
            ).with_distance_to(lat, lng)[:10]
        return self.with_distance_to(lat, lng)[:10]


class Company(d_m.Model):
    user = d_m.ForeignKey(User, verbose_name=_(u'manager'), null=True, blank=True,
                          related_name='companies')
    title = d_m.CharField(_(u'firm title'), max_length=200)  # firmenname
    region = d_m.CharField(_(u'region'), max_length=150, null=True, blank=True)  # bezirk
    city = d_m.CharField(_(u'city'), max_length=150, db_index=True)  # ort
    postal_code = d_m.CharField(_(u'postal code'), max_length=5, db_index=True)  # PLZ
    street_house = d_m.CharField(_(u'street and house'), max_length=150)  # straße
    phone = d_m.CharField(_(u'phone'), max_length=100, null=True, blank=True)  # telefon
    fax = d_m.CharField(_(u'fax'), max_length=100, null=True, blank=True)  # fax
    mail = d_m.EmailField(_(u'e-mail'), max_length=100, null=True, blank=True)  # emailadresse
    website = d_m.URLField(_(u'website'), null=True, blank=True)
    description = HTMLField(
        _(u'description'), null=True, blank=True,  # beschreibung
        help_text=_(u'Free text with predefined boilerplate '   # Freitext mit Vordefiniertem Textvorschlag,
                    u'that can be changed but'))  # welcher aber geändert werden kann
    logo = d_m.ImageField(_(u'logo'), upload_to=_make_upload_path, null=True, blank=True)
    slug = d_m.SlugField(editable=False, max_length=200)
    created = d_m.DateField(_(u'created'), editable=False, auto_now_add=True)  # erstellt
    updated = d_m.DateTimeField(_(u'updated'), editable=False, null=True)  # erneuerungsdatum
    latitude = d_m.FloatField(_(u'latitude'), editable=False, null=True)  # breite
    longitude = d_m.FloatField(_(u'longitude'), editable=False, null=True)  # länge
    uid = d_m.CharField(_(u'Firm UID'), editable=False, max_length=32, null=True, db_index=True)
    companies = CompaniesQuerySet.as_manager()

    class Meta:
        verbose_name = _(u'company')
        verbose_name_plural = _(u'companies')
        managed = True
        ordering = ('title',)

    def get_absolute_url(self):
        """
        @return: uri for detail page
        @rtype: str
        """
        if not self.slug:
            self.slug = slugify(self.title)
            self.save()
        return d_u.reverse(
            'catalog:detail',
            kwargs={'uid': self.uid, 'slug': self.slug})

    def get_contact_url(self):
        """
        @return: uri for contact data
        @rtype: str
        """
        if not self.slug:
            self.slug = slugify(self.title)
            self.save()
        return d_u.reverse(
            'catalog:firm_contact',
            kwargs={'uid': self.uid, 'slug': self.slug})

    def get_edit_url(self):
        """
        @return: uri for edit firm data
        @rtype: str
        """
        return d_u.reverse('firm_edit', kwargs={'pk': self.id})

    @property
    def pub_date(self):
        dt = self.updated or self.created
        tm = datetime.time.min
        return datetime.datetime.combine(dt, tm)

    @property
    def full_addr(self):
        addr = [self.street_house, self.postal_code,
                self.city, self.region, u'Deutschland']
        return u', '.join(i for i in addr if i)

    def full_addr_for_url(self):
        return unicodedata.normalize('NFKC', self.full_addr).encode('utf-8')

    @property
    def full_addr_url_encoded(self):
        return urllib.quote_plus(self.full_addr_for_url())

    @property
    def google_static_map_url(self, scheme='http'):
        params = settings.GOOGLE_MAP_GENERAL_PARAMS
        if self.latitude and self.longitude:
            mark = '{0:f},{1:f}'.format(self.latitude, self.longitude)
        else:
            mark = self.full_addr_for_url()
        params.update(markers=mark)
        uri = settings.GOOGLE_STATIC_MAP_PATH + '?' + urllib.urlencode(params)
        secret = settings.GOOGLE_MAP_SECRET
        if secret:
            decoded_key = base64.urlsafe_b64decode(secret)
            signature = hmac.new(decoded_key, uri, hashlib.sha1)
            encoded_signature = base64.urlsafe_b64encode(signature.digest())
            uri += '&signature=' + encoded_signature
        return scheme + '://' + settings.GOOGLE_STATIC_MAP_DOMAIN + uri

    @property
    def google_map_app_url(self):
        data = {
            'center': '{0:f},{1:f}'.format(self.latitude, self.longitude),
            'zoom': settings.GOOGLE_MAP_ZOOM,
            'q': self.full_addr_for_url()
        }
        return 'comgooglemaps://?' + urllib.urlencode(data)

    @property
    def google_de_map_url(self):
        return 'https://www.google.de/maps/place/{0}/'.format(self.full_addr_url_encoded)
