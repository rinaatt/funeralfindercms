# coding=utf-8
import django.db.models as d_m
from django.utils.translation import ugettext_lazy as _
from .company import Company


class OtherContact(d_m.Model):
    company = d_m.ForeignKey(
        Company, verbose_name=u'firm',
        related_name='other_contacts', editable=False)
    value = d_m.CharField(u'contact value', max_length=100)
    is_phone = d_m.BooleanField(u'is phone', default=False, db_index=True)
    is_mobile = d_m.BooleanField(u'is mobile phone', default=False, db_index=True)
    is_fax = d_m.BooleanField(u'is fax number', default=False, db_index=True)
    is_email = d_m.BooleanField(u'is e-mail address', default=False, db_index=True)

    class Meta:
        verbose_name = _(u'Other contact')
        verbose_name_plural = _(u'Other contacts')
