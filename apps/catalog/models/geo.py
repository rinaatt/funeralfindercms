# coding=utf-8
import django.db.models as d_m


class GeoFeatureClass(d_m.Model):
    id = d_m.CharField(u'class', max_length=1, primary_key=True)
    desc = d_m.CharField(u'description', max_length=100)


class GeoFeatureCode(d_m.Model):
    id = d_m.CharField(u'code', max_length=10, primary_key=True)
    cls = d_m.ForeignKey(GeoFeatureClass)
    title = d_m.CharField(max_length=50)
    desc = d_m.CharField(u'description', max_length=200, null=True)


class Country(d_m.Model):
    id = d_m.CharField(max_length=2, primary_key=True)
    title = d_m.CharField(max_length=100)

    def __unicode__(self):
        return self.title


class GeoNameQuerySet(d_m.QuerySet):
    def get_coordinates(self, name):
        place_qs = self.filter(
            d_m.Q(country__id='DE') &
            d_m.Q(feature_code__id__in=['PPL', 'PPLA', 'PPLA2', 'PPLA3', 'PPLA4']))
        try:
            place = place_qs.filter(name__iexact=name)[0]
        except IndexError:
            try:
                place = place_qs.filter(name__istartswith=name)[0]
            except IndexError:
                return None, None
        return place.latitude, place.longitude


class GeoName(d_m.Model):
    name = d_m.CharField(max_length=200, db_index=True)
    latitude = d_m.FloatField(u'breite', editable=False)
    longitude = d_m.FloatField(u'länge', editable=False)
    feature_class = d_m.ForeignKey(GeoFeatureClass)
    feature_code = d_m.ForeignKey(GeoFeatureCode)
    country = d_m.ForeignKey(Country)
    places = GeoNameQuerySet.as_manager()

    class Meta:
        index_together = ('country', 'name', 'feature_code',)
        ordering = ('name',)

    def __unicode__(self):
        return u'{1}, {0}'.format(self.country, self.name)

