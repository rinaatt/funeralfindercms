# coding=utf-8
import django.db.models as d_m
from .geo import Country


class PostalCodeQuerySet(d_m.QuerySet):
    def get_coordinates(self, code, place_name=None):
        lat, lng = None, None
        postals_qs = self.filter(code=code)
        if place_name is not None:
            place_qs = postals_qs \
                .filter(place_name__iexact=place_name) \
                .order_by('place_name')
            if not place_qs.exists():
                place_qs = postals_qs \
                    .filter(place_name__istartswith=place_name) \
                    .order_by('place_name')
            try:
                postal = place_qs[0]
            except IndexError:
                pass
            else:
                lat = postal.latitude
                lng = postal.longitude
        if lat is None or lng is None:
            postals_count = postals_qs.count()
            if postals_count > 1:
                lat, lng = 0.0, 0.0
                for postal in postals_qs:
                    lat += postal.latitude
                    lng += postal.longitude
                lat /= postals_count
                lng /= postals_count
            elif postals_count == 1:
                lat = postals_qs[0].latitude
                lng = postals_qs[0].longitude
        return lat, lng


class PostalCode(d_m.Model):
    country = d_m.ForeignKey(Country)
    code = d_m.CharField(max_length=20, db_index=True)
    place_name = d_m.CharField(max_length=180)
    latitude = d_m.FloatField(u'breite', editable=False)
    longitude = d_m.FloatField(u'länge', editable=False)
    accuracy = d_m.SmallIntegerField(help_text=u'accuracy of lat/lng from 1=estimated to 6=centroid')
    postal_codes = PostalCodeQuerySet.as_manager()

    class Meta:
        index_together = ['code', 'place_name', ]

    def __unicode__(self):
        return u'{0} {1}'.format(self.code, self.place_name)
