# coding=utf-8
from cms.models.pluginmodel import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from .forms import SearchForm
from .models import Company

__author__ = 'Rinat'


class SearchFormPlugin(CMSPluginBase):
    module = u'Catalog'
    name = u'Search form'
    # model = CMSPlugin
    render_template = "catalog/cms_plugins/search_form.html"

    def render(self, context, instance, placeholder):
        context['form'] = SearchForm()
        return context

plugin_pool.register_plugin(SearchFormPlugin)


class SearchByCharPlugin(CMSPluginBase):
    name = _(u'Search by char')
    render_template = 'catalog/cms_plugins/search_by_char.html'

    def render(self, context, instance, placeholder):
        context['firms_alphabet'] = Company.companies.get_alphabet()
        return context

plugin_pool.register_plugin(SearchByCharPlugin)
