# coding=utf-8


def get_setting(name):
    from django.conf import settings
    from meta_mixin import settings as meta_settings
    default = {
        'IMAGE_THUMBNAIL_SIZE': getattr(settings, 'ADVISOR_IMAGE_THUMBNAIL_SIZE',
                                        {'size': '120x120', 'crop': True, 'upscale': False}),
        'IMAGE_FULL_SIZE': getattr(settings, 'ADVISOR_IMAGE_FULL_SIZE',
                                   {'size': '640x120', 'crop': True, 'upscale': False}),
        'TAGCLOUD_MIN': getattr(settings, 'ADVISOR_TAGCLOUD_MIN', 1),
        'TAGCLOUD_MAX': getattr(settings, 'ADVISOR_TAGCLOUD_MAX', 10),
        'PAGINATION': getattr(settings, 'ADVISOR_PAGINATION', 10),
        'LATEST_POSTS': getattr(settings, 'ADVISOR_LATEST_POSTS', 5),
        'POSTS_LIST_TRUNCWORDS_COUNT': getattr(settings, 'ADVISOR_POSTS_LIST_TRUNCWORDS_COUNT', 100),
        'TYPE': getattr(settings, 'ADVISOR_TYPE', 'Article'),
        'FB_TYPE': getattr(settings, 'ADVISOR_FB_TYPE', 'Article'),
        'FB_APPID': getattr(settings, 'ADVISOR_FB_APPID', meta_settings.FB_APPID),
        'FB_PROFILE_ID': getattr(settings, 'ADVISOR_FB_PROFILE_ID', meta_settings.FB_PROFILE_ID),
        'FB_PUBLISHER': getattr(settings, 'ADVISOR_FB_PUBLISHER', meta_settings.FB_PUBLISHER),
        'FB_AUTHOR_URL': getattr(settings, 'ADVISOR_FB_AUTHOR_URL', 'get_author_url'),
        'FB_AUTHOR': getattr(settings, 'ADVISOR_FB_AUTHOR', 'get_author_name'),
        'TWITTER_TYPE': getattr(settings, 'ADVISOR_TWITTER_TYPE', 'Summary'),
        'TWITTER_SITE': getattr(settings, 'ADVISOR_TWITTER_SITE', meta_settings.TWITTER_SITE),
        'TWITTER_AUTHOR': getattr(settings, 'ADVISOR_TWITTER_AUTHOR', 'get_author_twitter'),
        'GPLUS_TYPE': getattr(settings, 'ADVISOR_GPLUS_SCOPE_CATEGORY', 'Advisor'),
        'GPLUS_AUTHOR': getattr(settings, 'ADVISOR_GPLUS_AUTHOR', 'get_author_gplus'),
        'ENABLE_COMMENTS': getattr(settings, 'ADVISOR_ENABLE_COMMENTS', True),
        'USE_ABSTRACT': getattr(settings, 'ADVISOR_USE_ABSTRACT', True),
        'USE_PLACEHOLDER': getattr(settings, 'ADVISOR_USE_PLACEHOLDER', True),
        'MULTISITE': getattr(settings, 'ADVISOR_MULTISITE', True),
        'AUTHOR_DEFAULT': getattr(settings, 'ADVISOR_AUTHOR_DEFAULT', True),
    }
    return default[name]
