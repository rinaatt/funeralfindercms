# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('advisor', '0013_auto_20150911_1247'),
    ]

    operations = [
        migrations.AlterField(
            model_name='posttranslation',
            name='post_text',
            field=ckeditor_uploader.fields.RichTextUploadingField(default=b'', verbose_name='text', blank=True),
        ),
    ]
