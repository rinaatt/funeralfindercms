# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('advisor', '0007_auto_20150719_0933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='authorentriesplugin',
            name='authors',
            field=models.ManyToManyField(related_name='advisor_posts', verbose_name='authors', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='authorentriesplugin',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='advisor_authorentriesplugin_rel', primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='latestpostsplugin',
            name='categories',
            field=models.ManyToManyField(help_text='Show only the blog articles tagged with chosen categories.', related_name='advisor_latest_posts', verbose_name='filter by category', to='advisor.AdvisorCategory', blank=True),
        ),
        migrations.AlterField(
            model_name='latestpostsplugin',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='advisor_latestpostsplugin_rel', primary_key=True, serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='latestpostsplugin',
            name='tags',
            field=models.ManyToManyField(help_text='Show only the blog articles tagged with chosen tags.', related_name='advisor_latest_posts', verbose_name='filter by tag', to='taggit.Tag', blank=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='enable_comments',
            field=models.BooleanField(default=False, verbose_name='enable comments on post'),
        ),
        migrations.AlterField(
            model_name='post',
            name='sites',
            field=models.ManyToManyField(help_text='Select sites in which to show the post. If none is set it will be visible in all the configured sites.', to='sites.Site', verbose_name='Site(s)', blank=True),
        ),
        migrations.AlterField(
            model_name='posttranslation',
            name='abstract',
            field=djangocms_text_ckeditor.fields.HTMLField(default=b'', verbose_name='abstract', blank=True),
        ),
    ]
