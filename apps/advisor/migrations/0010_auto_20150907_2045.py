# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cms.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('advisor', '0009_auto_20150907_2042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='content',
            field=cms.models.fields.PlaceholderField(related_name='advisor_post_content', slotname=b'post_content', editable=False, to='cms.Placeholder', null=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='sites',
            field=models.ManyToManyField(help_text='Select sites in which to show the post. If none is set it will be visible in all the configured sites.', related_name='advisor_posts', verbose_name='Site(s)', to='sites.Site', blank=True),
        ),
    ]
