# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advisor', '0011_auto_20150908_1251'),
    ]

    operations = [
        migrations.AlterField(
            model_name='authorentriesplugin',
            name='cms_plugin',
            field=models.OneToOneField(parent_link=True, related_name='advisor_authorentriesplugin_rel', primary_key=True, db_column=b'cmsplugin_ptr', serialize=False, to='cms.CMSPlugin'),
        ),
        migrations.AlterField(
            model_name='latestpostsplugin',
            name='cms_plugin',
            field=models.OneToOneField(parent_link=True, related_name='advisor_latestpostsplugin_rel', primary_key=True, db_column=b'cmsplugin_ptr', serialize=False, to='cms.CMSPlugin'),
        ),
    ]
