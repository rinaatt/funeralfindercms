# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advisor', '0008_auto_20150907_2039'),
    ]

    operations = [
        migrations.RenameField(
            model_name='authorentriesplugin',
            old_name='cmsplugin_ptr',
            new_name='cms_plugin',
        ),
        migrations.RenameField(
            model_name='latestpostsplugin',
            old_name='cmsplugin_ptr',
            new_name='cms_plugin',
        ),
    ]
