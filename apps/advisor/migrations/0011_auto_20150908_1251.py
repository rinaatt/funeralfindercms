# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advisor', '0010_auto_20150907_2045'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='advisorcategory',
            options={'verbose_name': 'adviser category', 'verbose_name_plural': 'adviser categories'},
        ),
        migrations.AlterModelOptions(
            name='advisorcategorytranslation',
            options={'default_permissions': (), 'verbose_name': 'adviser category Translation', 'managed': True},
        ),
    ]
