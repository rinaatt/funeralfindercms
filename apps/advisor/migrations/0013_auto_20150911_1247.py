# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('advisor', '0012_auto_20150911_1246'),
    ]

    operations = [
        migrations.RenameField(
            model_name='authorentriesplugin',
            old_name='cms_plugin',
            new_name='cmsplugin_ptr',
        ),
        migrations.RenameField(
            model_name='latestpostsplugin',
            old_name='cms_plugin',
            new_name='cmsplugin_ptr',
        ),
    ]
