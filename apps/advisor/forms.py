# coding=utf-8

from django import forms
from taggit_autosuggest.widgets import TagAutoSuggest


class LatestEntriesForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(LatestEntriesForm, self).__init__(*args, **kwargs)
        self.fields['tags'].widget = TagAutoSuggest('taggit.Tag')

    class Media:
        css = {
            'all': ('css/advisor_admin.css',)
        }


class SearchForm(forms.Form):
    query = forms.CharField(max_length=100, min_length=1)
