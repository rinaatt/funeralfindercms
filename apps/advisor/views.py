# coding=utf-8
from django.contrib.auth import get_user_model
from django.core.urlresolvers import resolve
from django.utils.timezone import now
from django.utils.translation import get_language
from django.views.generic import DetailView, ListView
from parler.views import TranslatableSlugMixin, ViewUrlMixin
from django.shortcuts import redirect
from django.db.models import Q
from django.http import Http404

from .models import ADVISOR_CURRENT_POST_IDENTIFIER, AdvisorCategory, Post
from .forms import SearchForm
from .settings import get_setting

User = get_user_model()


class BaseAdvisorView(ViewUrlMixin):
    request = None
    """@type: django.http.Request"""
    model = None
    """@type: django.db.models.Model"""

    def get_queryset(self):
        language = get_language()
        def_manager = getattr(self.model, '_default_manager')
        queryset = def_manager.active_translations(language_code=language)
        if not getattr(self.request, 'toolbar', False) or not self.request.toolbar.edit_mode:
            queryset = queryset.published()
        return queryset.on_site()

    def render_to_response(self, context, **response_kwargs):
        response_kwargs['current_app'] = resolve(self.request.path).namespace
        return super(BaseAdvisorView, self).render_to_response(context, **response_kwargs)


class BaseListView(BaseAdvisorView, ListView):
    model = Post
    context_object_name = 'post_list'
    template_name = 'advisor/post_list.html'
    paginate_by = get_setting('PAGINATION')

    def get_context_data(self, **kwargs):
        context = super(BaseListView, self).get_context_data(**kwargs)
        context['use_placeholder'] = get_setting('USE_PLACEHOLDER')
        context['use_abstract'] = get_setting('USE_ABSTRACT')
        context['TRUNCWORDS_COUNT'] = get_setting('POSTS_LIST_TRUNCWORDS_COUNT') or 100
        return context


class PostListView(BaseListView):
    view_url_name = 'advisor:posts-latest'

    def get_queryset(self):
        qs = super(PostListView, self).get_queryset()
        qs = qs.filter(categories__parent=None)
        return qs


class SearchListView(BaseListView):
    template_name = 'advisor/search_result.html'
    view_url_name = 'advisor:posts-search'
    form = None

    def get_queryset(self):
        from parler.utils import get_active_language_choices
        query_str = self.form.cleaned_data['query']
        qs = super(SearchListView, self).get_queryset()
        qs = qs.filter(
            Q(translations__language_code__in=get_active_language_choices()) & (
                Q(translations__title__contains=query_str) |
                Q(translations__post_text__contains=query_str)
            )
        )
        return qs

    def get(self, request, *args, **kwargs):
        """
        Http GET request hanler
        @param request: http request object
        @type request: django.http.request.HttpRequest
        @return: http response object
        @rtype: django.http.response.HttpResponse
        """
        self.form = SearchForm(request.GET)
        if not self.form.is_valid():
            return redirect('pages-root')
        # request.session['search_url'] = request.get_full_path()
        # request.session['last_query'] = self.form.cleaned_data['query']
        return super(SearchListView, self).get(request, *args, **kwargs)


class PostDetailView(TranslatableSlugMixin, BaseAdvisorView, DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'advisor/post_detail.html'
    slug_field = 'slug'
    view_url_name = 'advisor:post-detail'
    path_url_kwarg = 'path'

    def get_object(self, queryset=None):
        """
        Returns the object the view is displaying.

        By default this requires `self.queryset` and a `pk` or `slug` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        if queryset is None:
            queryset = self.get_queryset()
        path = self.kwargs.get(self.path_url_kwarg)
        slugs = path.split('/')
        slugs.reverse()
        categories = AdvisorCategory.objects.translated(slug=slugs[1])
        slug_field = self.get_slug_field()
        queryset = queryset\
            .filter(categories__in=categories)\
            .translated(**{slug_field: slugs[0]})
        try:
            obj = queryset.first()
        except queryset.model.DoesNotExist:
            raise Http404("No %(verbose_name)s found matching the query" %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def get(self, *args, **kwargs):
        # submit object to cms to get corrent language switcher and selected category behavior
        if hasattr(self.request, 'toolbar'):
            self.request.toolbar.set_object(self.get_object())
        return super(PostDetailView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['meta'] = self.get_object().as_meta()
        context['use_placeholder'] = get_setting('USE_PLACEHOLDER')
        setattr(self.request, ADVISOR_CURRENT_POST_IDENTIFIER, self.get_object())
        return context


class PostArchiveView(BaseListView):
    date_field = 'date_published'
    allow_empty = True
    allow_future = True
    view_url_name = 'advisor:posts-archive'

    def get_queryset(self):
        qs = super(PostArchiveView, self).get_queryset()
        if 'month' in self.kwargs:
            qs = qs.filter(**{'%s__month' % self.date_field: self.kwargs['month']})
        if 'year' in self.kwargs:
            qs = qs.filter(**{'%s__year' % self.date_field: self.kwargs['year']})
        return qs

    def get_context_data(self, **kwargs):
        kwargs['month'] = int(self.kwargs.get('month')) if 'month' in self.kwargs else None
        kwargs['year'] = int(self.kwargs.get('year')) if 'year' in self.kwargs else None
        if kwargs['year']:
            kwargs['archive_date'] = now().replace(kwargs['year'], kwargs['month'] or 1, 1)
        return super(PostArchiveView, self).get_context_data(**kwargs)


class TaggedListView(BaseListView):
    view_url_name = 'advisor:posts-tagged'

    def get_queryset(self):
        qs = super(TaggedListView, self).get_queryset()
        return qs.filter(tags__slug=self.kwargs['tag'])

    def get_context_data(self, **kwargs):
        kwargs['tagged_entries'] = (self.kwargs.get('tag')
                                    if 'tag' in self.kwargs else None)
        return super(TaggedListView, self).get_context_data(**kwargs)


class AuthorEntriesView(BaseListView):
    view_url_name = 'advisor:posts-authors'

    def get_queryset(self):
        qs = super(AuthorEntriesView, self).get_queryset()
        if 'username' in self.kwargs:
            qs = qs.filter(**{'author__%s' % User.USERNAME_FIELD: self.kwargs['username']})
        return qs

    def get_context_data(self, **kwargs):
        kwargs['author'] = User.objects.get(**{User.USERNAME_FIELD: self.kwargs.get('username')})
        return super(AuthorEntriesView, self).get_context_data(**kwargs)


class CategoryEntriesView(BaseListView):
    _category = None
    """@type: .models.AdvisorCategory"""
    view_url_name = 'advisor:posts-category'

    @property
    def category(self):
        if not self._category:
            self._category = AdvisorCategory.objects\
                .active_translations(get_language(), slug=self.kwargs['category'])\
                .latest('pk')
        return self._category

    def get(self, *args, **kwargs):
        # submit object to cms toolbar to get correct language switcher behavior
        if hasattr(self.request, 'toolbar'):
            self.request.toolbar.set_object(self.category)
        return super(CategoryEntriesView, self).get(*args, **kwargs)

    def get_queryset(self):
        qs = super(CategoryEntriesView, self).get_queryset()
        if 'category' in self.kwargs:
            qs = qs.filter(categories=self.category.pk)
        return qs

    def get_context_data(self, **kwargs):
        kwargs['category'] = self.category
        return super(CategoryEntriesView, self).get_context_data(**kwargs)
