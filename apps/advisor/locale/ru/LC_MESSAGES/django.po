# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Iacopo Spalletti, 2014
msgid ""
msgstr ""
"Project-Id-Version: djangocms-blog\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-09-17 13:36+0500\n"
"PO-Revision-Date: 2014-11-30 11:49+0000\n"
"Last-Translator: yakky <i.spalletti@nephila.it>\n"
"Language-Team: Russian (http://www.transifex.com/projects/p/djangocms-blog/"
"language/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#: .\apps\advisor\apps.py:9
msgid "Advisor"
msgstr ""

#: .\apps\advisor\cms_app.py:10 .\apps\advisor\cms_plugins.py:13
#: .\apps\advisor\cms_toolbar.py:17
msgid "Adviser"
msgstr ""

#: .\apps\advisor\cms_plugins.py:23 .\apps\advisor\cms_plugins.py:41
#, fuzzy
#| msgid "Latest Blog Articles"
msgid "Latest Advisor Articles"
msgstr "Последние статьи блога"

#: .\apps\advisor\cms_plugins.py:55
#, fuzzy
#| msgid "Author Blog Articles"
msgid "Author Advisor Articles"
msgstr "Автор"

#: .\apps\advisor\cms_plugins.py:69
msgid "Tags"
msgstr "Тэги"

#: .\apps\advisor\cms_plugins.py:80
msgid "Categories"
msgstr "Categories"

#: .\apps\advisor\cms_plugins.py:91
msgid "Archive"
msgstr "Архив"

#: .\apps\advisor\cms_plugins.py:102
msgid "Search form"
msgstr ""

#: .\apps\advisor\cms_toolbar.py:19
msgid "Post list"
msgstr "Список статей"

#: .\apps\advisor\cms_toolbar.py:21
msgid "Add post"
msgstr "Добавить статью"

#: .\apps\advisor\cms_toolbar.py:25
msgid "Edit Post"
msgstr ""

#: .\apps\advisor\feeds.py:17
#, fuzzy, python-format
#| msgid "Blog articles on %(site_name)s"
msgid "Advisor articles on %(site_name)s"
msgstr "Статьи из блог на %(site_name)s"

#: .\apps\advisor\menu.py:13
msgid "Adviser Category menu"
msgstr ""

#: .\apps\advisor\models.py:32
msgid "parent"
msgstr "предок"

#: .\apps\advisor\models.py:34
msgid "created at"
msgstr "время создания"

#: .\apps\advisor\models.py:35
msgid "modified at"
msgstr "время изменения"

#: .\apps\advisor\models.py:38
msgid "name"
msgstr "название"

#: .\apps\advisor\models.py:39 .\apps\advisor\models.py:127
msgid "slug"
msgstr "URL"

#: .\apps\advisor\models.py:46
#, fuzzy
#| msgid "category"
msgid "adviser category"
msgstr "категория"

#: .\apps\advisor\models.py:47
#, fuzzy
#| msgid "blog categories"
msgid "adviser categories"
msgstr "категории блога"

#: .\apps\advisor\models.py:92
#, fuzzy
#| msgid "Author"
msgid "author"
msgstr "Автор"

#: .\apps\advisor\models.py:95
#, fuzzy
#| msgid "created at"
msgid "created"
msgstr "время создания"

#: .\apps\advisor\models.py:96
#, fuzzy
#| msgid "modified at"
msgid "last modified"
msgstr "время изменения"

#: .\apps\advisor\models.py:97
#, fuzzy
#| msgid "Published Since"
msgid "published Since"
msgstr "Опубликована с"

#: .\apps\advisor\models.py:99
#, fuzzy
#| msgid "Published Until"
msgid "published Until"
msgstr "Опубликована до"

#: .\apps\advisor\models.py:101
#, fuzzy
#| msgid "Publish"
msgid "publish"
msgstr "Показывать на сайте"

#: .\apps\advisor\models.py:102
msgid "category"
msgstr "категория"

#: .\apps\advisor\models.py:104
#, fuzzy
#| msgid "Main image"
msgid "main image"
msgstr "Картинка для статьи"

#: .\apps\advisor\models.py:108
#, fuzzy
#| msgid "Main image thumbnail"
msgid "main image thumbnail"
msgstr "Уменьшенная копия"

#: .\apps\advisor\models.py:113
#, fuzzy
#| msgid "Main image full"
msgid "main image full"
msgstr "Полный размер"

#: .\apps\advisor\models.py:117
msgid "enable comments on post"
msgstr ""

#: .\apps\advisor\models.py:119
msgid "Site(s)"
msgstr ""

#: .\apps\advisor\models.py:121
msgid ""
"Select sites in which to show the post. If none is set it will be visible in "
"all the configured sites."
msgstr ""

#: .\apps\advisor\models.py:126
#, fuzzy
#| msgid "Title"
msgid "title"
msgstr "Заголовок"

#: .\apps\advisor\models.py:128
msgid "abstract"
msgstr ""

#: .\apps\advisor\models.py:129
msgid "post meta description"
msgstr ""

#: .\apps\advisor\models.py:131
msgid "post meta keywords"
msgstr ""

#: .\apps\advisor\models.py:133
msgid "post meta title"
msgstr ""

#: .\apps\advisor\models.py:134
msgid "used in title tag and social sharing"
msgstr ""

#: .\apps\advisor\models.py:137
msgid "text"
msgstr ""

#: .\apps\advisor\models.py:173
#, fuzzy
#| msgid "blog article"
msgid "advisor article"
msgstr "статья блога"

#: .\apps\advisor\models.py:174
#, fuzzy
#| msgid "0 articles"
msgid "advisor articles"
msgstr "0 статей"

#: .\apps\advisor\models.py:265 .\apps\advisor\models.py:298
#, fuzzy
#| msgid "0 articles"
msgid "articles"
msgstr "0 статей"

#: .\apps\advisor\models.py:266
msgid "The number of latests articles to be displayed."
msgstr "Количество показываемых последних статей."

#: .\apps\advisor\models.py:269
msgid "filter by tag"
msgstr ""

#: .\apps\advisor\models.py:270
msgid "Show only the blog articles tagged with chosen tags."
msgstr "Показывать только статьи с выбранными тэгами."

#: .\apps\advisor\models.py:273
#, fuzzy
#| msgid "blog category"
msgid "filter by category"
msgstr "категория блога"

#: .\apps\advisor\models.py:274
msgid "Show only the blog articles tagged with chosen categories."
msgstr "Показывать только статьи из выбранныех категорий."

#: .\apps\advisor\models.py:277
#, python-format
msgid "%s latest articles by tag"
msgstr ""

#: .\apps\advisor\models.py:294
#, fuzzy
#| msgid "Authors"
msgid "authors"
msgstr "Авторы"

#: .\apps\advisor\models.py:299
msgid "The number of author articles to be displayed."
msgstr "Количество статей автора, которые будут показаны."

#: .\apps\advisor\models.py:303
#, python-format
msgid "%s latest articles by author"
msgstr ""

#~ msgid "Blog"
#~ msgstr "Блог"

#~ msgid "blog articles"
#~ msgstr "статьи блога"

#~ msgid "Articles"
#~ msgstr "Статьи"

#~ msgid "by"
#~ msgstr "создана"

#~ msgid "Articles by"
#~ msgstr "Статьи созданы"

#~ msgid "Tag"
#~ msgstr "Тэг"

#~ msgid "Category"
#~ msgstr "Категория"

#~ msgid "No article found."
#~ msgstr "Не найдено ни одной статьи."

#~ msgid "Back"
#~ msgstr "Назад"

#~ msgid "previous"
#~ msgstr "предыдущая"

#~ msgid "Page"
#~ msgstr "Страница"

#~ msgid "of"
#~ msgstr "из"

#~ msgid "next"
#~ msgstr "следующая"

#~ msgid "read more"
#~ msgstr "продолжение"

#~ msgid "1 article"
#~ msgid_plural "%(articles)s articles"
#~ msgstr[0] "%(articles)s статья"
#~ msgstr[1] "%(articles)s статьи"
#~ msgstr[2] "%(articles)s статей"

#~ msgid "blog post"
#~ msgstr "blog post"

#~ msgid "Posts"
#~ msgstr "Posts"

#~ msgid "Entries by"
#~ msgstr "Entries by"

#~ msgid "No entry found."
#~ msgstr "No entry found."
