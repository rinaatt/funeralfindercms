# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import feeds as fd
from . import views as v

urlpatterns = [
    url(r'^$', v.PostListView.as_view(), name='posts-latest'),
    url(r'^feed/$', fd.LatestEntriesFeed(), name='posts-latest-feed'),
    url(r'^(?P<year>\d{4})/$', v.PostArchiveView.as_view(), name='posts-archive'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/$', v.PostArchiveView.as_view(), name='posts-archive'),
    url(r'^author/(?P<username>[\w\.@+-]+)/$', v.AuthorEntriesView.as_view(), name='posts-author'),
    url(r'^category/(?P<category>[\w\.@+-]+)/$', v.CategoryEntriesView.as_view(), name='posts-category'),
    url(r'^tag/(?P<tag>[-\w]+)/$', v.TaggedListView.as_view(), name='posts-tagged'),
    url(r'^tag/(?P<tag>[-\w]+)/feed/$', fd.TagFeed(), name='posts-tagged-feed'),
    url(r'^search$', v.SearchListView.as_view(), name='posts-search'),
    url(r'^(?P<path>.+)$', v.PostDetailView.as_view(), name='post-detail'),
]
