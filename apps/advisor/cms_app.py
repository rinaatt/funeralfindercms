# -*- coding: utf-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

from .menu import AdvisorCategoryMenu


class AdvisorApp(CMSApp):
    name = _('Adviser')
    urls = ['advisor.urls']
    app_name = 'advisor'
    menus = [AdvisorCategoryMenu]

apphook_pool.register(AdvisorApp)
