# coding=utf-8

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AdvisorConfig(AppConfig):
    name = 'advisor'
    verbose_name = _("Advisor")
