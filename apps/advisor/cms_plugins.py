# -*- coding: utf-8 -*-
from cms.models.pluginmodel import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from .forms import LatestEntriesForm, SearchForm
from .models import AuthorEntriesPlugin, AdvisorCategory, LatestPostsPlugin, Post
from .settings import get_setting


class AdvisorPlugin(CMSPluginBase):
    module = u'Ratgeber'
    model = CMSPlugin


class AdvisorLatestEntriesPlugin(AdvisorPlugin):
    """
    Non cached plugin which returns the latest posts taking into account the
      user / toolbar state
    """
    render_template = 'advisor/plugins/latest_entries.html'
    name = _('Latest Advisor Articles')
    model = LatestPostsPlugin
    form = LatestEntriesForm
    filter_horizontal = ('categories',)
    cache = False

    def render(self, context, instance, placeholder):
        context = super(AdvisorLatestEntriesPlugin, self).render(context, instance, placeholder)
        context['posts_list'] = instance.get_posts(context['request'])
        context['TRUNCWORDS_COUNT'] = get_setting('POSTS_LIST_TRUNCWORDS_COUNT')
        return context


class AdvisorLatestEntriesPluginCached(AdvisorPlugin):
    """
    Cached plugin which returns the latest published posts
    """
    render_template = 'advisor/plugins/latest_entries.html'
    name = _('Latest Advisor Articles')
    model = LatestPostsPlugin
    form = LatestEntriesForm
    filter_horizontal = ('categories',)

    def render(self, context, instance, placeholder):
        context = super(AdvisorLatestEntriesPluginCached, self).render(context, instance, placeholder)
        context['posts_list'] = instance.get_posts()
        context['TRUNCWORDS_COUNT'] = get_setting('POSTS_LIST_TRUNCWORDS_COUNT')
        return context


class AdvisorAuthorPostsPlugin(AdvisorPlugin):
    # module = _('Advisor')
    name = _('Author Advisor Articles')
    model = AuthorEntriesPlugin
    form = LatestEntriesForm
    render_template = 'advisor/plugins/authors.html'
    filter_horizontal = ['authors']

    def render(self, context, instance, placeholder):
        context = super(AdvisorAuthorPostsPlugin, self).render(context, instance, placeholder)
        context['authors_list'] = instance.get_authors()
        return context


class AdvisorTagsPlugin(AdvisorPlugin):
    # module = _('Advisor')
    name = _('Tags')
    render_template = 'advisor/plugins/tags.html'

    def render(self, context, instance, placeholder):
        context = super(AdvisorTagsPlugin, self).render(context, instance, placeholder)
        context['tags'] = Post.objects.tag_cloud(queryset=Post.objects.published())
        return context


class AdvisorCategoryPlugin(AdvisorPlugin):
    # module = _('Advisor')
    name = _('Categories')
    render_template = 'advisor/plugins/categories.html'

    def render(self, context, instance, placeholder):
        context = super(AdvisorCategoryPlugin, self).render(context, instance, placeholder)
        context['categories'] = AdvisorCategory.objects.all()
        return context


class AdvisorArchivePlugin(AdvisorPlugin):
    # module = _('Advisor')
    name = _('Archive')
    render_template = 'advisor/plugins/archive.html'

    def render(self, context, instance, placeholder):
        context = super(AdvisorArchivePlugin, self).render(context, instance, placeholder)
        context['dates'] = Post.objects.get_months(queryset=Post.objects.published())
        return context


class AdvisorSearchArticlePlugin(AdvisorPlugin):
    # module = _('Adviser')
    name = _('Search form')
    render_template = 'advisor/plugins/search_form.html'

    def render(self, context, instance, placeholder):
        context = super(AdvisorSearchArticlePlugin, self).render(context, instance, placeholder)
        context['form'] = SearchForm()
        return context


plugin_pool.register_plugin(AdvisorLatestEntriesPlugin)
plugin_pool.register_plugin(AdvisorAuthorPostsPlugin)
plugin_pool.register_plugin(AdvisorTagsPlugin)
plugin_pool.register_plugin(AdvisorArchivePlugin)
plugin_pool.register_plugin(AdvisorCategoryPlugin)
plugin_pool.register_plugin(AdvisorSearchArticlePlugin)
